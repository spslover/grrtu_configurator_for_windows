#pragma once




enum
{
	RW_READ,
	RW_WRITE, 
	RW_NONE
};



typedef struct _IDINFO 
{ 
 USHORT   wGenConfig;			// WORD 0: 基本信息字 
 USHORT   wNumCyls;				// WORD 1: 柱面数 
 USHORT   wReserved2;			// WORD 2: 保留 
 USHORT   wNumHeads;			// WORD 3: 磁頭數  
 USHORT   wReserved4;			// WORD 4: 保留 
 USHORT   wReserved5;			// WORD 5: 保留 
 USHORT   wNumSectorsPerTrack;  // WORD 6: 每磁道扇區數 
 USHORT   wVendorUnique[3];		// WORD 7-9: 廠家設定值 
 CHAR     sSerialNumber[20];	// WORD 10-19:序列號 
 USHORT   wBufferType;			// WORD 20: 缓衝類型 
 USHORT   wBufferSize;			// WORD 21: 缓衝大小 
 USHORT   wECCSize;				// WORD 22: ECC校驗大小 
 CHAR     sFirmwareRev[8];		// WORD 23-26: 固件版本 
 CHAR     sModelNumber[40];		// WORD 27-46: 内部型號 
 USHORT   wMoreVendorUnique;	// WORD 47: 廠家設定值 
 USHORT   wReserved48;			// WORD 48: 保留 

 struct { 
	USHORT   reserved1:8; 
	USHORT   DMA:1;     // 1=支持DMA 
	USHORT   LBA:1;     // 1=支持LBA 
	USHORT   DisIORDY:1;    // 1=可不使用IORDY 
	USHORT   IORDY:1;    // 1=支持IORDY 
	USHORT   SoftReset:1;   // 1=需要ATA軟啟動 
	USHORT   Overlap:1;    // 1=支持重疊操作 
	USHORT   Queue:1;    // 1=支持命令隊列 
	USHORT   InlDMA:1;    // 1=支持交叉存取DMA 
	} wCapabilities;     // WORD 49: 一般能力 

 USHORT   wReserved1;     // WORD 50: 保留 
 USHORT   wPIOTiming;     // WORD 51: PIO時序 
 USHORT   wDMATiming;     // WORD 52: DMA時序 
 
 struct { 
	USHORT   CHSNumber:1;   // 1=WORD 54-58有效 
	USHORT   CycleNumber:1;   // 1=WORD 64-70有效 
	USHORT   UnltraDMA:1;   // 1=WORD 88有效 
	USHORT   reserved:13; 
	} wFieldValidity;     // WORD 53: 後續字段有效性標誌 

 USHORT   wNumCurCyls;    // WORD 54: CHS可尋址的柱面數 
 USHORT   wNumCurHeads;    // WORD 55: CHS可尋址的磁頭數 
 USHORT   wNumCurSectorsPerTrack;  // WORD 56: CHS可尋址每磁道扇區數 
 USHORT   wCurSectorsLow;    // WORD 57: CHS可尋址的扇區數低位字 
 USHORT   wCurSectorsHigh;   // WORD 58: CHS可尋址的扇區數高位字 
 
 struct { 
	USHORT   CurNumber:8;   // 當前一次性可讀寫扇區數 
	USHORT   Multi:1;    // 1=已選擇多扇區讀寫 
	USHORT   reserved1:7; 
	} wMultSectorStuff;     // WORD 59: 多扇區讀寫設定 
 ULONG   dwTotalSectors;    // WORD 60-61: LBA可尋址的扇區數 
 USHORT   wSingleWordDMA;    // WORD 62: 單字節DMA支持能力  
 
 struct { 
 USHORT   Mode0:1;    // 1=支持模式0 (4.17Mb/s) 
 USHORT   Mode1:1;    // 1=支持模式1 (13.3Mb/s) 
 USHORT   Mode2:1;    // 1=支持模式2 (16.7Mb/s) 
 USHORT   Reserved1:5; 
 USHORT   Mode0Sel:1;    // 1=已選擇模式0 
 USHORT   Mode1Sel:1;    // 1=已選擇模式1 
 USHORT   Mode2Sel:1;    // 1=已選擇模式2 
 USHORT   Reserved2:5; 
 } wMultiWordDMA;     // WORD 63: 多字節DMA支持能力 
 
 struct { 
	USHORT   AdvPOIModes:8;   // 支持高级POI模式數 
	USHORT   reserved:8; 
	} wPIOCapacity;      // WORD 64: 高级PIO支持能力 

 USHORT   wMinMultiWordDMACycle;  // WORD 65: 多字節DMA傳輸周期的最小值 
 USHORT   wRecMultiWordDMACycle;  // WORD 66: 多字節DMA傳輸周期的建議值 
 USHORT   wMinPIONoFlowCycle;   // WORD 67: 無流控制時PIO傳輸周期的最小值 
 USHORT   wMinPOIFlowCycle;   // WORD 68: 有流控制時PIO傳輸周期的最小值 
 USHORT   wReserved69[11];   // WORD 69-79: 保留 
 
 struct { 
	USHORT   Reserved1:1; 
	USHORT   ATA1:1;     // 1=支持ATA-1 
	USHORT   ATA2:1;     // 1=支持ATA-2 
	USHORT   ATA3:1;     // 1=支持ATA-3 
	USHORT   ATA4:1;     // 1=支持ATA/ATAPI-4 
	USHORT   ATA5:1;     // 1=支持ATA/ATAPI-5 
	USHORT   ATA6:1;     // 1=支持ATA/ATAPI-6 
	USHORT   ATA7:1;     // 1=支持ATA/ATAPI-7 
	USHORT   ATA8:1;     // 1=支持ATA/ATAPI-8 
	USHORT   ATA9:1;     // 1=支持ATA/ATAPI-9 
	USHORT   ATA10:1;    // 1=支持ATA/ATAPI-10 
	USHORT   ATA11:1;    // 1=支持ATA/ATAPI-11 
	USHORT   ATA12:1;    // 1=支持ATA/ATAPI-12 
	USHORT   ATA13:1;    // 1=支持ATA/ATAPI-13 
	USHORT   ATA14:1;    // 1=支持ATA/ATAPI-14 
	USHORT   Reserved2:1; 
	} wMajorVersion;     // WORD 80: 主版本 

 USHORT   wMinorVersion;    // WORD 81: 副版本 
 USHORT   wReserved82[6];    // WORD 82-87: 保留 

 struct { 
	USHORT   Mode0:1;    // 1=支持模式0 (16.7Mb/s) 
	USHORT   Mode1:1;    // 1=支持模式1 (25Mb/s) 
	USHORT   Mode2:1;    // 1=支持模式2 (33Mb/s) 
	USHORT   Mode3:1;    // 1=支持模式3 (44Mb/s)   
	USHORT   Mode4:1;    // 1=支持模式4 (66Mb/s) 
	USHORT   Mode5:1;    // 1=支持模式5 (100Mb/s) 
	USHORT   Mode6:1;    // 1=支持模式6 (133Mb/s) 
	USHORT   Mode7:1;    // 1=支持模式7 (166Mb/s) ??? 
	USHORT   Mode0Sel:1;    // 1=已選擇模式0 
	USHORT   Mode1Sel:1;    // 1=已選擇模式1 
	USHORT   Mode2Sel:1;    // 1=已選擇模式2 
	USHORT   Mode3Sel:1;    // 1=已選擇模式3 
	USHORT   Mode4Sel:1;    // 1=已選擇模式4 
	USHORT   Mode5Sel:1;    // 1=已選擇模式5 
	USHORT   Mode6Sel:1;    // 1=已選擇模式6 
	USHORT   Mode7Sel:1;    // 1=已選擇模式7 
	} wUltraDMA;      // WORD 88:   Ultra DMA支持能力 
 USHORT     wReserved89[11];   // WORD 89-99
 QWORD		TotalNumberOfLogicalSectors; //WORD 100-103
 USHORT     wReserved104[13];   // WORD 104-116
 DWORD		LogicalSectorSize;	// WORD 117-118
 USHORT     wReserved119[137];  // WORD 119-255 

} IDINFO, *PIDINFO; 

typedef	struct _IDENTIFY_DEVICE_OUTDATA
{
	SENDCMDOUTPARAMS	SendCmdOutParam;
	BYTE				Data[IDENTIFY_BUFFER_SIZE - 1];
} IDENTIFY_DEVICE_OUTDATA, *PIDENTIFY_DEVICE_OUTDATA;

BOOL GetAPI_Identify(HANDLE hDevice, IDINFO *deviceInfo);
BOOL GetATA_Identify(HANDLE hDevice, IDINFO* pIdenInfo);	
BOOL GetSCSI_ATA_Identify(HANDLE hDevice, IDINFO *deviceInfo);
BOOL GetSCSI_Identify(HANDLE hDevice, IDINFO *deviceInfo);

BOOL GetReadSector(HANDLE hDevice, DWORD dwLBA, BYTE SectorCnt,BYTE *pBuffer);

BOOL UpdateATAStrings(HANDLE hDevice, char *pSerialNumber, char *pFW_Version, char* pModelNumber);
BOOL GetFlashStatus(HANDLE hDevice, BYTE function, BYTE *pBuf);
BOOL SendATAPassThrough48BITS(HANDLE hDevice, BYTE bATAFlag, 
							  BYTE FeatureCurrent, BYTE SectorCurrent, DWORD dwLBACurrent, BYTE Device, BYTE Command, 
							  BYTE FeaturePrevious, BYTE SectorPrevious, DWORD dwLBAPrevious, 							  
							   WORD wLength, BYTE *pBuf);

BOOL SendScsiCmdATAPassTroughCDB12(BYTE CDB12[] , 
									 HANDLE	hDevice , BYTE  lpBuf[] , BYTE DataInOut, DWORD *pdwBufferLen);

BOOL SendScsiCmdATAPassTroughCDB16(BYTE CDB16[] , 
									 HANDLE	hDevice , BYTE  lpBuf[] , BYTE DataInOut, DWORD *pdwBufferLen);
