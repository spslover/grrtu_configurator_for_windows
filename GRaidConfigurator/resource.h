//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GRaidConfigurator.rc
//
#define IDD_GRAIDCONFIGURATOR_DIALOG    102
#define IDD_RAIDMODE_DLG                103
#define IDR_MAINFRAME                   128
#define IDC_CONFIGURE_BTN               1000
#define IDC_DEVICE_CB                   1002
#define IDC_DEVICE_ST                   1003
#define IDC_NAME_ST                     1006
#define IDC_NAME_CB                     1007
#define IDC_TYPE_ST                     1008
#define IDC_TYPE_VALUE_ST               1009
#define IDC_CAPACITY_ST                 1010
#define IDC_CAPACITY_VALUE_ST           1011
#define IDC_STATUS_ST                   1012
#define IDC_STATUS_VALUE_ST             1013
#define IDC_DISKS_ST                    1014
#define IDC_DISKS_VALUE_ST              1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
