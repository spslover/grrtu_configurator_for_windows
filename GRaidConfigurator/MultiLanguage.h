
#define ENGLISH	0
#define CHINESE	1

#define ENG_MAIN_DLG_MESSAGE_MESSAGE			"Message"
#define CHI_MAIN_DLG_MESSAGE_MESSAGE			"訊息"

#define ENG_MAIN_DLG_CONTORL_LIST_CONTROL		"control"
#define CHI_MAIN_DLG_CONTORL_LIST_CONTROL		"控制"

#define ENG_MAIN_DLG_FIRMWARE_VERSION			"Firmware Version:"
#define CHI_MAIN_DLG_FIRMWARE_VERSION			"韌 體 版 本 :"

#define ENG_MAIN_DLG_FIRMWARE_VERSION_NO_DEVICE	"Please select the device!"
#define CHI_MAIN_DLG_FIRMWARE_VERSION_NO_DEVICE	"請選擇裝置!"

//////////////////////////////////////////////////////////////////
#define ENG_MAIN_DLG_MESSAGE_RAID_MODE			"RAID Mode:"
#define ENG_MAIN_DLG_MESSAGE_VHDD_REASON		"VHDD Reason:"
#define ENG_MAIN_DLG_MESSAGE_RAID_STATUS		"RAID Status:"
#define ENG_MAIN_DLG_MESSAGE_TEMPERATURE		"Temperature:"
#define ENG_MAIN_DLG_MESSAGE_DEVICE_SN			"Device S/N:"
#define ENG_MAIN_DLG_MESSAGE_FAN_SPEED			"Fan Speed:"
#define ENG_MAIN_DLG_MESSAGE_DEVICE_CAPACITY	"Device Capacity:"
#define ENG_MAIN_DLG_MESSAGE_PORT_0				"Port 0:"
#define ENG_MAIN_DLG_MESSAGE_PORT_1				"Port 1:"
#define ENG_MAIN_DLG_MESSAGE_PORT_2				"Port 2:"

#define CHI_MAIN_DLG_MESSAGE_RAID_MODE			"RAID 模式:"
#define CHI_MAIN_DLG_MESSAGE_VHDD_REASON		"虛擬硬碟原因:"
#define CHI_MAIN_DLG_MESSAGE_RAID_STATUS		"RAID 狀態:"
#define CHI_MAIN_DLG_MESSAGE_TEMPERATURE		"溫度:"
#define CHI_MAIN_DLG_MESSAGE_DEVICE_SN			"裝置序號:"
#define CHI_MAIN_DLG_MESSAGE_FAN_SPEED			"風扇速度:"
#define CHI_MAIN_DLG_MESSAGE_DEVICE_CAPACITY	"裝置容量:"
#define CHI_MAIN_DLG_MESSAGE_PORT_0				"連結埠 0:"
#define CHI_MAIN_DLG_MESSAGE_PORT_1				"連結埠 1:"
#define CHI_MAIN_DLG_MESSAGE_PORT_2				"連結埠 2:"

//////////////////////////////////////////////////////////////////
#define ENG_MODE_CHANGE_DLG_CURRENT_MODE	"Current Mode:"
#define ENG_MODE_CHANGE_DLG_DISK_AMOUNT		"Disk Amount:"
#define ENG_MODE_CHANGE_DLGE_CHANGE_MODE	"Change Mode:"

#define CHI_MODE_CHANGE_DLG_CURRENT_MODE	"現在模式:"
#define CHI_MODE_CHANGE_DLG_DISK_AMOUNT		"硬碟數量:"
#define CHI_MODE_CHANGE_DLG_CHANGE_MODE		"切換模式:"

#define ENG_MODE_CHANGE_DLG_MODE_CHANGE_FINISH	"Mode Change finished!"
#define ENG_MODE_CHANGE_DLG_MODE_CHANGE_FAIL	"Mode Change fail!"
#define ENG_MODE_CHANGE_DLG_MODE_CHANGE_SELECT  "Please select mode!"


#define CHI_MODE_CHANGE_DLG_MODE_CHANGE_FINISH	"切換模式完成!"
#define CHI_MODE_CHANGE_DLG_MODE_CHANGE_FAIL	"切換模式失敗!"
#define CHI_MODE_CHANGE_DLG_MODE_CHANGE_SELECT  "請選擇模式!"


//////////////////////////////////////////////////////////////////
#define ENG_PSUDO_DEVICE_STR_CAPABILITIES_00		"Supported"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIES_FF		"Not Support"

#define CHI_PSUDO_DEVICE_STR_CAPABILITIES_00		"支援"
#define CHI_PSUDO_DEVICE_STR_CAPABILITIES_FF		"不支援"
//////////////////////////////////////////////////////////////////
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_00		"Port Multiplier"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_01		"RAID 0"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_02		"RAID 1"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_03		"SPAN"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_04		"Side Winder"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_05		"Split Volumes Mode"
#define ENG_PSUDO_DEVICE_STR_CAPABILITIE_CURRENT_MODE_0F		"Virtual HDD"

//////////////////////////////////////////////////////////////////
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_00		"Port Multiplier"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_01		"RAID 0"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_02		"RAID 1"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_03		"SPAN"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_04		"Side Winder"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_05		"Split Volumes Mode"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_CURRENT_MODE_0F		"Virtual HDD"
//////////////////////////////////////////////////////////////////
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_00		"Good"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_01		"Degraded"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_02		"Rebuilding"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_03		"Rebuild Failed"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_04		"Data Lost"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_05		"Migrating"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_06		"Verifying"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_07		"Verify Failed"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_08		"Rebuilding Paused"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_09		"Verifying Paused"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_10		"Not Configured"	
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_11		"Broken"	

#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_00		"良好"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_01		"降級"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_02		"重建中"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_03		"重建失敗"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_04		"資料遺失"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_05		"遷移中"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_06		"驗證中"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_07		"驗證失敗"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_08		"重建暫停"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_09		"驗證暫停"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_10		"尚未配置"	
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_11		"損毀"

//////////////////////////////////////////////////////////////////
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDMODE_00		"Port Multiplier"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDMODE_01		"RAID 0"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDMODE_02		"RAID 1"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDMODE_03		"SPAN"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDMODE_10		"Not Configured"
//////////////////////////////////////////////////////////////////
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_00	"Good"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_01	"Rebuilding"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_02	"Empty"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_03	"Missing"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_04	"Blank"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_05	"ID Mismatch"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_06	"Rejected"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_07	"Predictive Failure"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_08	"Failed"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_09	"Spare"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0A	"Not Participating"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0B	"Verifying"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0C	"Migrating"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_11	"Rebuilding Pause"
#define ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_1B	"Verifying Pause"

#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_00	"良好"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_01	"重建中"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_02	"無裝置"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_03	"遺失"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_04	"被遮掩"				//
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_05	"名稱不合"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_06	"拒絕"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_07	"預測性故障"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_08	"失敗"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_09	"備用"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0A	"未參與"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0B	"確認中"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0C	"搬移中"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_11	"重建暫停"
#define CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_1B	"確認暫停"

//////////////////////////////////////////////////////////////////
#define ENG_CURRENT_VHDD_REASON_00		"None ATA disk connected"
#define ENG_CURRENT_VHDD_REASON_10		"RAID 0 crashed"
#define ENG_CURRENT_VHDD_REASON_11		"SPANcrashed"
#define ENG_CURRENT_VHDD_REASON_12		"RAID 1 disks mismatched"
#define ENG_CURRENT_VHDD_REASON_13		"RAID 1 missing"
#define ENG_CURRENT_VHDD_REASON_14		"Side Winder combination of disks mismatched"
#define ENG_CURRENT_VHDD_REASON_15		"Side Winder missing"
#define ENG_CURRENT_VHDD_REASON_20		"RAID type mismatched"
#define ENG_CURRENT_VHDD_REASON_30		"FORCE VHDD command"

#define CHI_CURRENT_VHDD_REASON_00		"沒有連結ATA裝置"
#define CHI_CURRENT_VHDD_REASON_10		"RAID 0 毀壞"
#define CHI_CURRENT_VHDD_REASON_11		"SPAN毀壞"
#define CHI_CURRENT_VHDD_REASON_12		"RAID 1 硬碟不符合"
#define CHI_CURRENT_VHDD_REASON_13		"RAID 1 遺失"
#define CHI_CURRENT_VHDD_REASON_14		"Side Winder 硬碟不符合"
#define CHI_CURRENT_VHDD_REASON_15		"Side Winder 遺失"
#define CHI_CURRENT_VHDD_REASON_20		"RAID 型態不符合"
#define CHI_CURRENT_VHDD_REASON_30		"FORCE VHDD 指令"

//////////////////////////////////////////////////////////////////





class MultiLanguageStr{
public:
	MultiLanguageStr();
	~MultiLanguageStr();
	
	void setLanguageEng();
	void setLanguageChi();

public:
	CString strMainDlgMessageMessage;
	CString strMainDlgFirmwareVersion;
	CString strMainDlgFirmareVersionNoDevice;
	CString strMainDlgContorlListControl;
	CString strMainDlgMessageVHDDReason;

	CString strMainDlgMessageRaidMode;
	CString strMainDlgMessageRaidStatus;
	CString strMainDlgMessageTemperature;
	CString strMainDlgMessageDeviceSN;
	CString strMainDlgMessageFanSpeed;
	CString strMainDlgMessageDeviceCapacity;
	CString strMainDlgMessagePort0;
	CString strMainDlgMessagePort1;
	CString strMainDlgMessagePort2;
	CString strMainDlgMessageDeviceName0;
	CString strMainDlgMessageDeviceName1;
	CString strMainDlgMessageDeviceName2;
	
	CString strModeChangeDlgCurrentMode;
	CString strModeChangeDlgDiskAmount;
	CString strModeChangeDlgChangeMode;

	CString strModeChangeDlgChangeFinish;
	CString strModeChangeDlgChangeFail;
	CString strModeChangeDlgChangeSelect;

	CString	strPsudoDeviceCapabilities00;
	CString strPsudoDeviceCapabilitiesFF;
	
	CString strPsudoDevicRaidStatusRaidStatus00;
	CString strPsudoDevicRaidStatusRaidStatus01;
	CString strPsudoDevicRaidStatusRaidStatus02;
	CString strPsudoDevicRaidStatusRaidStatus03;
	CString strPsudoDevicRaidStatusRaidStatus04;
	CString strPsudoDevicRaidStatusRaidStatus05;
	CString strPsudoDevicRaidStatusRaidStatus06;
	CString strPsudoDevicRaidStatusRaidStatus07;
	CString strPsudoDevicRaidStatusRaidStatus08;
	CString strPsudoDevicRaidStatusRaidStatus09;
	CString strPsudoDevicRaidStatusRaidStatus10;
	CString strPsudoDevicRaidStatusRaidStatus11;	

	CString strPsudoDevicRaidStatusPortStatus00;
	CString strPsudoDevicRaidStatusPortStatus01;
	CString strPsudoDevicRaidStatusPortStatus02;
	CString strPsudoDevicRaidStatusPortStatus03;
	CString strPsudoDevicRaidStatusPortStatus04;
	CString strPsudoDevicRaidStatusPortStatus05;
	CString strPsudoDevicRaidStatusPortStatus06;
	CString strPsudoDevicRaidStatusPortStatus07;
	CString strPsudoDevicRaidStatusPortStatus08;
	CString strPsudoDevicRaidStatusPortStatus09;
	CString strPsudoDevicRaidStatusPortStatus0A;
	CString strPsudoDevicRaidStatusPortStatus0B;
	CString strPsudoDevicRaidStatusPortStatus0C;
	CString strPsudoDevicRaidStatusPortStatus11;
	CString strPsudoDevicRaidStatusPortStatus1B;

	CString strCurrentVhddReason00;
	CString strCurrentVhddReason10;
	CString strCurrentVhddReason11;
	CString strCurrentVhddReason12;
	CString strCurrentVhddReason13;
	CString strCurrentVhddReason14;
	CString strCurrentVhddReason15;
	CString strCurrentVhddReason20;
	CString strCurrentVhddReason30;

};