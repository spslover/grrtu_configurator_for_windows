// RaidModeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GRaidConfigurator.h"
#include "RaidModeDlg.h"
#include "afxdialogex.h"


// CRaidModeDlg dialog

IMPLEMENT_DYNAMIC(CRaidModeDlg, CDialog)

CRaidModeDlg::CRaidModeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRaidModeDlg::IDD, pParent)
{

}

CRaidModeDlg::~CRaidModeDlg()
{
}

void CRaidModeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CRaidModeDlg, CDialog)
END_MESSAGE_MAP()


// CRaidModeDlg message handlers
