/*************************************************************************************
*Description:
*   Commands of ASM2109 Application - Firmware Interface (transfer by Jimmy)
*
*History:
*   2012/10/30  V1.6    Jimmy Hu	    Add Buffer Read/Write in test tool.
*   2012/10/04  V1.5    Jesse Chang     Add argument of absolute address flag in Read/Write/Erase flash rom commands
*                                       Add BIST START, BIST STOP, GET CAPABILITIES commands
*   2012/09/06  V1.4    Jesse Chang     Modify GetAckData function delay(100) -> delay(READ_ACK_INTERVAL)
*   2012/08/30  V1.3    Jesse Chang     Modify every function for PACKET and ACKDATA structure modified to sync. hardware design
*   2012/08/29  V1.2    Jesse Chang     Modify Header LBA to little endian to sync. hardware design
*   2012/08/28  V1.1    Jesse Chang     Modify GetAckData compare signature bug.
*   2012/07/30  V1.0    Jesse Chang     First version
*
*                                                   Asmedia Technology, Inc
***************************************************************************************/

#include "stdafx.h"
#include "Command.h"
#include "CRC.h"
#include "outputString.h"


#define COMMAND_RETRY_COUNT				3 
#define TOTAL_READ_BYTES            (124*1024)//(60*1024)               //Total read 60KB
#define READ_BYTES_EACH_TIME        (4*1024)                //Read 4KB each time

extern CButton SequentialMethod;
extern CButton BufferMethod;
extern DEVICE_TABLE ui_device[DEVICE_NUM];

int total_time[5];
int m_CtlRadioSelectATAHD_1;

unsigned char m_FWfile[TOTAL_READ_BYTES];
//::memset(m_FWfile,0,TOTAL_READ_BYTES);
BYTE	g_bDiskCnt = 0;
BYTE	g_b109DevCnt =0 ;
BYTE	g_bAsm109List[DRIVE_COUNT];
BYTE	g_Flag = 0;
DWORD	g_FWSize = TOTAL_READ_BYTES;
UINTN	g_RetryCountMax = 0;
UINTN	g_TotalRetryCount = 0;
UINTN	g_ERetryCount;
UINTN	g_ERetryCountMax;



cmdPacket::cmdPacket(){
	pCmdPack = new COMMANDPACKET;
	devicePath = "";
	hDevice = NULL;
	commandType=COMMAND_SEQENTIAL;

}
cmdPacket::~cmdPacket(){

	delete(pCmdPack);
}


void cmdPacket::clearPathHandle(void){
	if(hDevice != NULL){
		CloseHandle(hDevice);
		hDevice = NULL;
	}
	
	if(devicePath != ""){
		devicePath = "";
	}
}



void    cmdPacket::setHandle		(CString path){
	
	if(hDevice != NULL){
		CloseHandle(hDevice);
		hDevice = NULL;
	}

	hDevice =  CreateFile(	path,    // device interface name
										GENERIC_READ | GENERIC_WRITE,       // dwDesiredAccess
										FILE_SHARE_READ | FILE_SHARE_WRITE, // dwShareMode
										NULL,                               // lpSecurityAttributes
										OPEN_EXISTING,                      // dwCreationDistribution
										0,                                  // dwFlagsAndAttributes
										NULL                                // hTemplateFile
										);		
	sectorSize =512;
}

void cmdPacket::setHandle(int deviceOrder){
	
	if(hDevice != NULL){
		CloseHandle(hDevice);
		hDevice = NULL;
	}

	if(ui_device[deviceOrder].path != ""){
		hDevice =  CreateFile(	ui_device[deviceOrder].path,    // device interface name
											GENERIC_READ | GENERIC_WRITE,       // dwDesiredAccess
											FILE_SHARE_READ | FILE_SHARE_WRITE, // dwShareMode
											NULL,                               // lpSecurityAttributes
											OPEN_EXISTING,                      // dwCreationDistribution
											0,                                  // dwFlagsAndAttributes
											NULL                                // hTemplateFile
											);	
	}	
	sectorSize =512;
}


IFSTATUS cmdPacket::IFCmdIdentify(void){
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    //PIFDATA         pData;                  //Packet data 128 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;


//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
    pType->CascadeLayer = CASCADE_LAYER_NA;                 //for which cascade layer
    pType->CascadeLogicPort = CASCADE_PORT_NA;              //for which cascade logic port
    //Command Field
    pCommand->Command = IDENTIFY;                           //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field
//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
		return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(IDENTIFY_TIMEOUT);
    if(IFStatus != IFSuccess)
    {	
		return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFReceived)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }
}


IFSTATUS cmdPacket::SendPacket(void)
{
    UINT32*     pLBA;           //Physical LBA array
    UINTN       element;        //How many LBA must be sent
    UINTN       i;
    UINT16*     pWord;          //For Header LBA
    UINT16      HeaderLBA;
    UINT8*      pByte;

	pLBA = pCmdPack->pLBA;
    element = 0;
    //Change packet to physical LBA number
    //1. Change Header to LBA, Packet has 4 UINT16 little endian Header LBA
    pWord = (UINT16*)&pCmdPack->Packet.Header;
    for(i=0; i<HEADER_NUMBER; i++)
    {
        HeaderLBA = pWord[i];
        //BTLExchange((UINT8*)&HeaderLBA, 2);       //V1.1
        pLBA[i] = (UINT32)HeaderLBA;
    }
    element = HEADER_NUMBER;

    //2. Change Type to LBA, Packet has 4 bytes type
    pByte = (UINT8*)&pCmdPack->Packet.Type;
    for(i=0; i<TYPE_NUMBER; i++)
    {
        pLBA[element+i] = (UINT32)pByte[i] + TYPE_LBA_BASE;
    }
    element += TYPE_NUMBER;

    //3. Change Command to LBA, Packet has 4 bytes command
    pByte = (UINT8*)&pCmdPack->Packet.Command;
    for(i=0; i<COMMAND_NUMBER; i++)
    {
        pLBA[element+i] = (UINT32)pByte[i] + COMMAND_LBA_BASE;
    }
    element += COMMAND_NUMBER;
    
    //4. Change Data to LBA, Packet has 128 bytes data
    if(pCmdPack->DataField == TRUE)
    {
        pByte = (UINT8*)&pCmdPack->Packet.Data;
        for(i=0; i<DATA_NUMBER; i++)
        {
            pLBA[element+i] = (UINT32)pByte[i] + DATA_LBA_BASE;
        }
        element += DATA_NUMBER;
        //5. Change CRC to LBA, Packet has 4 bytes CRC
        pByte = (UINT8*)&pCmdPack->Packet.DataCrc;
        for(i=0; i<CRC_NUMBER; i++)
        {
            pLBA[element+i] = (UINT32)pByte[i] + CRC_LBA_BASE;
        }
        element += CRC_NUMBER;
    }
    else
    {
        //5. Change CRC to LBA, Packet has 4 bytes CRC
        pByte = (UINT8*)&pCmdPack->Packet.NonDataCrc;
        for(i=0; i<CRC_NUMBER; i++)
        {
            pLBA[element+i] = (UINT32)pByte[i] + CRC_LBA_BASE;
        }
        element += CRC_NUMBER;
    }

    pCmdPack->LBAElement = element;
    return(SendPhysicalLBA());
}



void cmdPacket::BTLExchange(UINT8* pData, UINTN Length)
{
    UINTN   i;
    UINT8   tmp;

    for(i=0; i<(Length/2); i++){
        tmp = pData[i];
        pData[i] = pData[Length-1-i];
        pData[Length-1-i] = tmp;
    }
    return;
}


IFSTATUS cmdPacket::SendPhysicalLBA	(){
	WORD	wLenSector = 1;
	DWORD	dwLBA	   = 0;
	int		dev		   = 0;

	UINT32 temp =0x00;
	for(UINT i=0 ; i<(UINT)(pCmdPack->LBAElement) ; i++){		
		DWORD dwLen = wLenSector * 512;
		if(pCmdPack->pLBA[i] == temp){
				if(!sendLBA(0x5FFF,	hDevice , pCmdPack->pData)){
		
					return IFFailure;
				}
			}

			if(!sendLBA(pCmdPack->pLBA[i],	hDevice , pCmdPack->pData)){
				return IFFailure;
			}else{
				temp = pCmdPack->pLBA[i];
			}	
	}
	return IFSuccess;
}

CString cmdPacket::getStatusEx(void){
	CString result;
	result.Format("0x%X\n", excutionStatus);
	return result;
}

CString cmdPacket::getAckLBAEx(void){
	CString result;
	result.Format("0x%lX\n", pCmdPack->AckLBA);
	return result;
}


CString cmdPacket::getAckStatusEx(void){
	CString result;
	result.Format("0x%X\n", pCmdPack->AckStatus);
	return result;
}

CString cmdPacket::getLBAElementEx(void){
	CString result;
	result.Format(" %d \n", pCmdPack->LBAElement);
	return result;
}


CString  cmdPacket::getLBAEx(void){

	int i=pCmdPack->LBAElement;
	UINT32*         pLBA; 
	CString result, temp;
	
	result="";
	pLBA = pCmdPack->pLBA;
    while(i)
	{
		temp.Format("%04X    ", *pLBA++);
        result += temp;
		i--;
    }
	return result;
}

CString cmdPacket::getString (void){
	return cStr;
}


IFSTATUS cmdPacket::ReadAck(PCOMMANDPACKET pCmdPack)
{
    UINTN       DriveHandle;
    UINT8*      pAckData;
    IFSTATUS    status = IFFailure;


    DriveHandle = pCmdPack->DriveHandle;
    pAckData = pCmdPack->pData;
	
	WORD	wLenSector = 1;
	DWORD	dwLBA	   = 0;
	int		dev		   = 0;

	if(!sendLBA(pCmdPack->AckLBA,	hDevice , pAckData))
	{		
		status = IFReadHDDFail;
	}
	else
	{
		status = IFSuccess;
	}

    return status;
}


IFSTATUS cmdPacket::GetAckData(UINTN WaitTime)
{
    clock_t     ticks1, ticks2;
    BOOL        Timeout;
    BOOL        GetAck;
    PIFACKDATA  pAckData;
    IFSTATUS    IFStatus;

    Timeout = FALSE;
    GetAck = FALSE;
    ticks1 = clock();
    pAckData = (PIFACKDATA)pCmdPack->pData;
    pCmdPack->AckLBA = ACKLBA;

    while((Timeout == FALSE) && (GetAck == FALSE))
    {
        sendLBA(0x5FFF,	hDevice , pCmdPack->pData);
     
        IFStatus = ReadAck(pCmdPack);           //Read ACK LBA
        if(IFStatus != IFSuccess)
        {
            return IFStatus;
        }
        
        //Compare ACK Signature
        if((memcmp(pAckData->HeadSignature, HEAD_SIGNATURE, HEAD_SIGNATURE_LENGTH) == 0) && \
            (memcmp(pAckData->TailSignature, TAIL_SIGNATURE, TAIL_SIGNATURE_LENGTH) == 0))
        {
            GetAck = TRUE;
        }
        else
        {
            ticks2 = clock();
            if((UINT)(ticks2/CLOCKS_PER_SEC - ticks1/CLOCKS_PER_SEC) > (UINT)(WaitTime/CLOCKS_PER_SEC))
            {
                Timeout = TRUE;
            }
        }
		Sleep(5);//delay READ_ACK_INTERVAL ms
    }
    if(GetAck == TRUE)
    {
        pCmdPack->AckStatus = GetAckStatus(pAckData->Status);


        return IFSuccess;
    }
    else
    {
        pCmdPack->AckStatus = IFAckTimeout;


        return IFAckTimeout;
    }
 }

IFSTATUS cmdPacket::GetAckStatus(UINT8 status)
{
    IFSTATUS    ACKStatus;

    switch(status)
    {
        case CMD_RECEIVED:                  //Command received
            ACKStatus = IFReceived;
            break;
        case CMD_INPROCESS:                 //Command in process
            ACKStatus = IFInprocess;
            break;
        case CMD_COMPLETE:		            //Command complete without error
            ACKStatus = IFComplete;
            break;
        case CMD_BLOCKED:			        //Command is blocked and can't be executed
            ACKStatus = IFBlock;
            break;
        case CMD_MISMATCH:			        //Subcommand mismatched
            ACKStatus = IFMismatch;
            break;
        case CMD_CRC_ERROR:		            //CRC error
            ACKStatus = IFCrcError;
            break;
        case CMD_UNSUPPORTED:		        //Command unsupported
            ACKStatus = IFUnsupported;
            break;
        case CMD_FAILURE:			        //Command failure
            ACKStatus = IFFailure;
            break;
        default:
            ACKStatus = IFInvalidStatus;
            break;
    }
    return ACKStatus;
}

void cmdPacket::FillOutPacketHeader(PIFHEADER pHeader)
{
	//Header Field
    pHeader->HeaderLBA0 = HEADERLBA0;
    pHeader->HeaderLBA1 = HEADERLBA1;
    pHeader->HeaderLBA2 = HEADERLBA2;
    pHeader->HeaderLBA3 = HEADERLBA3;
 
    return;
}


IFSTATUS cmdPacket::IFCmdReadFlashRom(UINT8 CascadeLayer, UINT8 CascadePort, UINT32 RomAddress, UINT8* pReadData, BOOL AbsoluteAddr)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    //PIFDATA         pData;                //Packet data 128 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    UINT16          SequenceNumber;
    PIFACKDATA      pAckData;

//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }


    //Rom Address is greater than supported? valid Rom address? Address must be a multiple of 128
    if(((RomAddress/DATA_NUMBER)>MAXIMUM_SEQUENCE_NUMBER) || ((RomAddress%DATA_NUMBER)!=0))
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
    //Command Field
    pCommand->Command = READ_FLASH_ROM;                     //Command
	pCommand->Subcommand = (AbsoluteAddr == TRUE) ? ABSOLUTE_ADDRESS : OFFSET_ADDRESS;  //Subcommand

    SequenceNumber = (UINT16)(RomAddress / DATA_NUMBER);
    BTLExchange((UINT8*)&SequenceNumber, sizeof(UINT16));   //Change to big endian
    pCommand->SequenceNumber = SequenceNumber;              //Sequence number
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy(pReadData, (UINT8*)(&pAckData->Packet.Data), DATA_NUMBER); 
            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdEraseFlashRomSector(UINT8 CascadeLayer, UINT8 CascadePort, UINT32 RomAddress, BOOL AbsoluteAddr){
	PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    UINT16          SequenceNumber;
 

//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }


    //Rom Address is greater than supported? valid Rom address? Address must be 4KB alignment
    if(((RomAddress/DATA_NUMBER)>MAXIMUM_SEQUENCE_NUMBER) || ((RomAddress%FLASH_ROM_SECTOR)!=0))
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
    //Command Field
    pCommand->Command = ERASE_FLASH_ROM_SECTOR;             //Command
	pCommand->Subcommand = (AbsoluteAddr == TRUE) ? ABSOLUTE_ADDRESS : OFFSET_ADDRESS;      //Subcommand
    SequenceNumber = (UINT16)(RomAddress / DATA_NUMBER);
    BTLExchange((UINT8*)&SequenceNumber, sizeof(UINT16));   //Change to big endian
    pCommand->SequenceNumber = SequenceNumber;              //Sequence number
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;

        }
        else
        {
            return IFSuccess;
        }
    }

}
IFSTATUS cmdPacket::IFCmdWriteFlashRom(UINT8 CascadeLayer, UINT8 CascadePort, UINT32 RomAddress, UINT8* pWriteData, BOOL AbsoluteAddr){
	PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFDATA         pData;                  //Packet data 128 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    UINT16          SequenceNumber;
//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }
    //Rom Address is greater than supported? valid Rom address? Address must be a multiple of 128
    if(((RomAddress/DATA_NUMBER)>MAXIMUM_SEQUENCE_NUMBER) || ((RomAddress%DATA_NUMBER)!=0))
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pData = &pCmdPack->Packet.Data;
    pCrc = &pCmdPack->Packet.DataCrc;                       //Data packet
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITH_DATA_FIELD;                     //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
    //Command Field
    pCommand->Command = WRITE_FLASH_ROM;                    //Command
    //pCommand->Subcommand = SUBCMD_NA;                     //Subcommand
    pCommand->Subcommand = (AbsoluteAddr == TRUE) ? ABSOLUTE_ADDRESS : OFFSET_ADDRESS;  //Subcommand
    SequenceNumber = (UINT16)(RomAddress / DATA_NUMBER);
    BTLExchange((UINT8*)&SequenceNumber, sizeof(UINT16));   //Change to big endian
    pCommand->SequenceNumber = SequenceNumber;              //Sequence number
    //Data Field
    memcpy((UINT8*)pData, pWriteData, DATA_NUMBER);
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITH_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = TRUE;                             //With Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdReset(UINT8 CascadeLayer, UINT8 CascadePort, UINT16 DisconnectTime){
	PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;

//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE){
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;

    //Command Field
    pCommand->Command = RESET;                              //Command
    pCommand->Subcommand = SUBCMD_NA; 
	pCommand->SequenceNumberLSB = (DisconnectTime& 0x00FF);//Subcommand
	pCommand->SequenceNumberMSB = (DisconnectTime& 0xFF00)>>8;

	//CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess){
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(DisconnectTime);
    if(IFStatus != IFSuccess){
        return IFStatus;
    }
    else{
        if(pCmdPack->AckStatus != IFReceived){
            return pCmdPack->AckStatus;
        }
        else{
            return IFSuccess;
        }
    }

}

IFSTATUS cmdPacket::IFCmdGetRegister(UINT8 CascadeLayer, UINT8 CascadePort, UINT16 RegAddress, UINT8* pRegValue){
	PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is lttle endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    UINT16          SequenceNumber;
    PIFACKDATA      pAckData;
    //UINT8           Value;

//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }
//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
    //Command Field
    pCommand->Command = GET_REGISTER;                       //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    SequenceNumber = RegAddress;
    BTLExchange((UINT8*)&SequenceNumber, sizeof(UINT16));   //Change to big endian
    pCommand->SequenceNumber = SequenceNumber;              //Sequence number
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;
//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field
//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            *pRegValue = *((UINT8*)&pAckData->Packet.Data); 
            return IFSuccess;
        }
    }
}


IFSTATUS cmdPacket::IFCmdSetRegister(UINT8 CascadeLayer, UINT8 CascadePort, UINT16 RegAddress, UINT8 RegValue){
	 PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    UINT16          SequenceNumber;

//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }
//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
    //Command Field
    pCommand->Command = SET_REGISTER;                       //Command
    pCommand->Subcommand = RegValue;                        //Subcommand
    SequenceNumber = RegAddress;
    BTLExchange((UINT8*)&SequenceNumber, sizeof(UINT16));   //Change to big endian
    pCommand->SequenceNumber = SequenceNumber;              //Sequence number
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field
//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }

}

bool	cmdPacket::sendLBA(DWORD shiftLBA, HANDLE hDevice, BYTE  lpBuf[]){
	
	DWORD	dwReadByteRet;
	DWORD	dwErrorCode;
	BYTE	pBuffer[65536];
	BOOL	bResult;
											
	if(INVALID_HANDLE_VALUE == hDevice)
	{
                CloseHandle(hDevice);
				hDevice =NULL;
				return 0;
	}

	SetFilePointer (hDevice, sectorSize*shiftLBA, NULL, FILE_BEGIN);
	if(lpBuf != NULL){
		try{
			memset(lpBuf, 0x00,  512);
		}catch(...){
		
		}
	}

	if(!ReadFile(hDevice, pBuffer, sectorSize, &dwReadByteRet, NULL)){
		dwErrorCode = ::GetLastError();
		return FALSE;
	}else{
		bResult = TRUE; 
		memcpy( lpBuf , pBuffer , sectorSize);
		return TRUE;	
	}
}

IFSTATUS cmdPacket::IFCmdBistStart(UINT8 CascadeLayer, UINT8 CascadePort, UINT8 DevicePorts, UINT8 SATAGenerations, UINT8 TestTime)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;

//Check parameters
	if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }
//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
	//Command Field
    pCommand->Command = BIST_START;                         //Command
    pCommand->Subcommand = DevicePorts;                     //Subcommand
    pCommand->SequenceNumberMSB = SATAGenerations;          //Sequence number MSB
    pCommand->SequenceNumberLSB = TestTime;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFReceived)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdBistStop(UINT8 CascadeLayer, UINT8 CascadePort, PBISTRESULTS pBistResults)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    PIFACKDATA      pAckData;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
    pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
 
	//Command Field
    pCommand->Command = BIST_STOP;                          //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy((UINT8*)pBistResults, (UINT8*)(&pAckData->Packet.Data), sizeof(BISTRESULTS)); 
            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdGetCapabilities(UINT8 CascadeLayer, UINT8 CascadePort, PCAPABILITIES pCap)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    PIFACKDATA      pAckData;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;

	//Command Field
    pCommand->Command = GET_CAPABILITIES;                          //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy((UINT8*)pCap, (UINT8*)(&pAckData->Packet.Data), sizeof(CAPABILITIES)); 
            return IFSuccess;
        }
    }
    return IFUnsupported;
}

IFSTATUS cmdPacket::IFCmdModeChange(UINT8 CascadeLayer, UINT8 CascadePort, PRAIDPARAMETER pRaidParameter, UINT16 disconnectTime)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFDATA         pData;                  //Packet data 128 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pData = &pCmdPack->Packet.Data;
    pCrc = &pCmdPack->Packet.DataCrc;                       //Data packet
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITH_DATA_FIELD;                     //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
	//Command Field
    pCommand->Command = MODE_CHANGE;                        //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberLSB = (disconnectTime& 0x00FF);		//Subcommand,BigEndin
	pCommand->SequenceNumberMSB = (disconnectTime& 0xFF00)>>8;
	//Data Field
    memcpy((UINT8*)pData, (UINT8*)pRaidParameter, sizeof(RAIDPARAMETER));
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITH_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = TRUE;                             //With Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdGetRAIDStatus(UINT8 CascadeLayer, UINT8 CascadePort, PRAIDSTATUS pRaidStatus)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    PIFACKDATA      pAckData;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
    pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
	//Command Field
    pCommand->Command = GET_RAID_STATUS;                    //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy((UINT8*)pRaidStatus, (UINT8*)(&pAckData->Packet.Data), sizeof(RAIDSTATUS)); 
												

				pRaidStatus->VolumeStatus[0].BytesPerBlock = pRaidStatus->VolumeStatus[0].BytesPerBlock << 8 | pRaidStatus->VolumeStatus[0].BytesPerBlock >> 8 ;
				{//UINT64  TotalBlocks;                                    //Total logic blocks (sectors) of volume, big endian
					UINT64 *x = &pRaidStatus->VolumeStatus[0].TotalBlocks ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);
				}

				{//UINT64	StartLBA;										//Start absolute LBA;
					UINT64 *x = &pRaidStatus->VolumeStatus[0].StartLBA ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);
				}

				{//UINT64	EndLBA;											//End absolute LBA;
					UINT64 *x = &pRaidStatus->VolumeStatus[0].EndLBA ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);
				}

				{//UINT64	TagLBA;			
					UINT64 *x = &pRaidStatus->VolumeStatus[0].TagLBA ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);
				}

            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdGetDiskStatus(UINT8 CascadeLayer, UINT8 CascadePort, PDISKSTATUS pDiskStatus)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    PIFACKDATA      pAckData;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
   
	//Command Field
    pCommand->Command = GET_DISKS_STATUS;                   //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy((UINT8*)pDiskStatus, (UINT8*)(&pAckData->Packet.Data), sizeof(DISKSTATUS)); 


	//			UINT64  TotalBlocks;                                    //Total logic blocks (sectors) of volume, big endian
				pDiskStatus->PortStatus[0].ModelNumber[39]= '\0';
				pDiskStatus->PortStatus[1].ModelNumber[39]= '\0';
				pDiskStatus->PortStatus[2].ModelNumber[39]= '\0';


				for(int j=0 ;j<3 ;j++){
			
					for(int i=0 ;i <37 ;i++){
						if(pDiskStatus->PortStatus[j].ModelNumber[i] == ' '){
							if(pDiskStatus->PortStatus[j].ModelNumber[i+1] == ' '){
								if(pDiskStatus->PortStatus[j].ModelNumber[i+2] == ' '){
									if(pDiskStatus->PortStatus[j].ModelNumber[i+3] == ' '){
										pDiskStatus->PortStatus[j].ModelNumber[i] = '\0';
									}
								}						
							}
						}
					}
				}

				pDiskStatus->PortStatus[0].BytesPerBlock = pDiskStatus->PortStatus[0].BytesPerBlock << 8 | pDiskStatus->PortStatus[0].BytesPerBlock >> 8 ;
				pDiskStatus->PortStatus[1].BytesPerBlock = pDiskStatus->PortStatus[1].BytesPerBlock << 8 | pDiskStatus->PortStatus[1].BytesPerBlock >> 8 ;
				pDiskStatus->PortStatus[2].BytesPerBlock = pDiskStatus->PortStatus[2].BytesPerBlock << 8 | pDiskStatus->PortStatus[2].BytesPerBlock >> 8 ;

				UINT8 temp;
				for(int i=0; i<40 ;i= i+2 ){
					temp = pDiskStatus->PortStatus[0].ModelNumber[i];
					pDiskStatus->PortStatus[0].ModelNumber[i] = pDiskStatus->PortStatus[0].ModelNumber[i+1];
					pDiskStatus->PortStatus[0].ModelNumber[i+1] = temp;

					temp = pDiskStatus->PortStatus[1].ModelNumber[i];
					pDiskStatus->PortStatus[1].ModelNumber[i] = pDiskStatus->PortStatus[1].ModelNumber[i+1];
					pDiskStatus->PortStatus[1].ModelNumber[i+1] = temp;

					temp = pDiskStatus->PortStatus[2].ModelNumber[i];
					pDiskStatus->PortStatus[2].ModelNumber[i] = pDiskStatus->PortStatus[2].ModelNumber[i+1];
					pDiskStatus->PortStatus[2].ModelNumber[i+1] = temp;
				}

				for(int i=0; i<20 ;i= i+2 ){
					temp = pDiskStatus->PortStatus[0].SerialNumber[i];
					pDiskStatus->PortStatus[0].SerialNumber[i] = pDiskStatus->PortStatus[0].SerialNumber[i+1];
					pDiskStatus->PortStatus[0].SerialNumber[i+1] = temp;

					temp = pDiskStatus->PortStatus[1].SerialNumber[i];
					pDiskStatus->PortStatus[1].SerialNumber[i] = pDiskStatus->PortStatus[1].SerialNumber[i+1];
					pDiskStatus->PortStatus[1].SerialNumber[i+1] = temp;

					temp = pDiskStatus->PortStatus[1].SerialNumber[i];
					pDiskStatus->PortStatus[2].SerialNumber[i] = pDiskStatus->PortStatus[1].SerialNumber[i+1];
					pDiskStatus->PortStatus[2].SerialNumber[i+1] = temp;
				}

		


			{//UINT64	TotalBlocks;			
					UINT64 *x = &pDiskStatus->PortStatus[0].TotalBlocks ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);

					x = &pDiskStatus->PortStatus[1].TotalBlocks ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);

					 x = &pDiskStatus->PortStatus[2].TotalBlocks ;

					*x = (*x>>56) |
						 ((*x<<40) & 0x00FF000000000000) |
						 ((*x<<24) & 0x0000FF0000000000) |
						 ((*x<<8)  & 0x000000FF00000000) |
						 ((*x>>8)  & 0x00000000FF000000) |
						 ((*x>>24) & 0x0000000000FF0000) |
						 ((*x>>40) & 0x000000000000FF00) |
						 (*x<<56);
			}



            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdRebuild(UINT8 CascadeLayer, UINT8 CascadePort, UINT8 Operation, UINT8 Volume){
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }
//Check operation
    if((Operation != REBUILD_START) && (Operation != REBUILD_STOP) && (Operation != REBUILD_PAUSE) && (Operation != REBUILD_RESUME))
    {
        return IFInvalidParameter;
    }
//Check Volume, Max. 4 volumes
    if(Volume > REBUILD_MAXVOLUMES)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
 
	//Command Field
    pCommand->Command = REBUILD;                            //Command
    pCommand->Subcommand = Operation;                       //Subcommand
    pCommand->SequenceNumberMSB = Volume;                   //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData( COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFReceived)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }
}

IFSTATUS cmdPacket::IFCmdVerify(UINT8 CascadeLayer, UINT8 CascadePort, UINT8 Operation, UINT8 Volume){

    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }
//Check operation
    if((Operation != VERIFY_START) && (Operation != VERIFY_STOP) && (Operation != VERIFY_PAUSE) && (Operation != VERIFY_RESUME))
    {
        return IFInvalidParameter;
    }
//Check Volume, Max. 4 volumes
    if(Volume > VERIFY_MAXVOLUMES)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = SET_COMMAND;                       //Get/Set command
	pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
  
	//Command Field
    pCommand->Command = _VERIFY_;                            //Command
    pCommand->Subcommand = Operation;                       //Subcommand
    pCommand->SequenceNumberMSB = Volume;                   //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFReceived)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            return IFSuccess;
        }
    }
}
IFSTATUS cmdPacket::IFCmdGetEnclosureStatus(UINT8 CascadeLayer, UINT8 CascadePort, PENCLOSURESTATUS pEnclosureStatus){
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    PIFACKDATA      pAckData;

//Check parameters
    if(CheckCascadeParameters(CascadeLayer, CascadePort) != TRUE)
    {
        return IFInvalidParameter;
    }

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
    pType->CascadeLayer = (CascadeLayer == IGNORE_CASCADE) ? (CASCADE_LAYER_NA) : CascadeLayer;
    pType->CascadeLogicPort = (CascadePort == IGNORE_CASCADE) ? (CASCADE_PORT_NA) : CascadePort;
  
	//Command Field
    pCommand->Command = GET_ENCLOSURE_STATUS;               //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy((UINT8*)pEnclosureStatus, (UINT8*)(&pAckData->Packet.Data), sizeof(ENCLOSURESTATUS)); 
            return IFSuccess;
        }
    }
}

void cmdPacket::clearBuffer(void){

	memset(pCmdPack->pData, 0x00, sizeof(UINT8)*512);
	memset(pCmdPack->pLBA , 0x00, sizeof(UINT8)*512);

	memset(&pCmdPack->Packet, 0x00, sizeof(pCmdPack->Packet));
	memset(pCmdPack, 0x00, sizeof(pCmdPack));
	cStr="";
	return ;
}


BOOL cmdPacket::CheckCascadeParameters(UINT8 CascadeLayer, UINT8 CascadePort)
{
    UINT8   Layer;
    UINT8   L1Port;
    UINT8   L2Port;

    if (CascadeLayer != IGNORE_CASCADE)
    {
        Layer = CascadeLayer >> 4;                                  //CAL bit 7:4 - layer 
        if(Layer < MAXIMUM_CASCADE_LAYER)                           //valid layer?
        {                                                           //Check CAP.L2P and CAP.L1P
            L1Port = CascadePort & 0x0F;                            //CAP bit 3:0 - L1P
            L2Port = CascadePort >> 4;                              //CAP bit 7:4 - L2P
            if((L1Port > MAXIMUM_CASCADE_PORT) || (L2Port > MAXIMUM_CASCADE_PORT))
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
        else
        {
            return FALSE;
        }
    }
    return TRUE;
}

IFSTATUS cmdPacket::IFCmdGetTopology(PTOPOLOGY pTopology)
{
    PIFHEADER       pHeader;                //Packet header 8 bytes, LBA is little endian
    PIFTYPE         pType;                  //Packet type 4 bytes
    PIFCOMMAND      pCommand;               //Packet command 4 bytes
    PIFCRC          pCrc;                   //Packet crc 4 bytes, crc is big endian
    PIFPACKET       pPacket;                //Packet
    UINT32          CrcValue;
    IFSTATUS        IFStatus;
    PIFACKDATA      pAckData;

//Fill out pCmdPack->Packet
    pPacket = &pCmdPack->Packet;
    pHeader = &pCmdPack->Packet.Header;
    pType = &pCmdPack->Packet.Type;
    pCommand = &pCmdPack->Packet.Command;
    pCrc = &pCmdPack->Packet.NonDataCrc;
    //Header Field
    FillOutPacketHeader(pHeader);
    //Type Field
    pType->DataField = WITHOUT_DATA_FIELD;                  //with/without Data Field
    pType->CommandType = GET_COMMAND;                       //Get/Set command
    pType->CascadeLayer = CASCADE_LAYER_NA;
    pType->CascadeLogicPort = CASCADE_PORT_NA;
    //Command Field
    pCommand->Command = GET_TOPOLOGY;                       //Command
    pCommand->Subcommand = SUBCMD_NA;                       //Subcommand
    pCommand->SequenceNumberMSB = SEQHI_NA;                 //Sequence number MSB
    pCommand->SequenceNumberLSB = SEQLO_NA;                 //Sequence number LSB
    //CRC field
    CrcValue = GetCrc32((UINT8*)pPacket, CRC_LENGTH_WITHOUT_DATA);
    BTLExchange((UINT8*)&CrcValue, sizeof(UINT32));         //Change to big endian
    pCrc->Crc = CrcValue;

//pCmdPack->DataField
    pCmdPack->DataField = FALSE;                            //Without Data field

//Send command packet to ASM2109
    IFStatus = SendPacket();
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
//Get Acknowledge
    IFStatus = GetAckData(COMMAND_TIMEOUT);
    if(IFStatus != IFSuccess)
    {
        return IFStatus;
    }
    else
    {
        if(pCmdPack->AckStatus != IFComplete)
        {
            return pCmdPack->AckStatus;
        }
        else
        {
            pAckData = (PIFACKDATA)pCmdPack->pData;
            memcpy((UINT8*)pTopology, (UINT8*)(&pAckData->Packet.Data), sizeof(TOPOLOGY)); 
            return IFSuccess;
        }
    }
}