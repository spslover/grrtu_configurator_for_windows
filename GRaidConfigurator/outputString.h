#pragma once

extern UINT8 config_1[];
extern UINT8 config_2[];
extern UINT8 config_3[];
extern UINT8 node_B[];
extern UINT8 node_G[];

extern void initialStatusSTR(void);


extern BYTE    g_DeviceNum;
extern BYTE	   g_CurrDeviceNum;

extern HANDLE  g_hDevNotify	;
extern HWND	g_hWnd			;

extern CString statusSTR[][0xff+1];



typedef struct deviceInfo{
	UINT32 ID;
}deviceInfo;

/* statusSTR */
#define STR_CAPABILITIES_TEMPERATURE		0x01
#define STR_CAPABILITIES_FANSPEEDREPORT		0x02
#define STR_CAPABILITIES_CURRENTMODE		0x03

#define STR_RAIDSTATUS_CURRENTMODE			0x11
#define STR_RAIDSTATUS_RAIDSTATUS			0x12
#define STR_RAIDSTATUS_RAIDMODE				0x13
#define STR_RAIDSTATUS_STRIPESIZE			0x14

#define	STR_DISKSTATUS_CURRENTMODE			0x21
#define	STR_DISKSTATUS_PORTSTATUS			0x22

#define STR_CURRENT_VHDD_REASON				0x31


// showMessage
#define MESSAGE_TEMPERATURE				1		//				-getCapabilities
#define MESSAGE_FANSPEED				2		//				-getCapabilities
#define MESSAGE_REBUILDING_PERCENTAGE	3
#define MESSAGE_VHDD_REASON				4
#define MESSAGE_DEVICE_MN				9		//model number	-getDiskStatus
#define MESSAGE_DEVICE_SN				10		//serial number	-getDiskStatus
#define MESSAGE_DEVICE_SIZE				11		//				-getRAIDStatus
#define MESSAGE_PORT_0_MODEL_NUM			12
#define MESSAGE_PORT_1_MODEL_NUM			13
#define MESSAGE_PORT_2_MODEL_NUM			14


#define MESSAGE_CAPACITY_DEVICE			22
#define MESSAGE_CURRENTMODE				25		//				-getCapabilities
#define MESSAGE_RAID_STATUS				26


#define FIRMWARE_FILE_LENGTH_64KBIMG    61440L              //(60*1024)ASM2109 firmware file legth must be 60KB for 64KB image
#define WRITE_BYTES_EACH_TIME           4096L               //(4*1024)Write 4KB each time
#define COMMAND_RETRY_COUNT				3   