#pragma once

#include "stdafx.h"
#include "outputString.h"
#include "MultiLanguage.h"

CString g_strDevicePath[16]={};
CString g_strDeviceName[16]={};
BYTE    g_DeviceNum =0;
HANDLE  g_hDevNotify;

CString statusSTR[0xff][0xff+1];

extern MultiLanguageStr MultiSTING;

void initialStatusSTR(void){
	/*CAPABILITIES*/
	statusSTR[STR_CAPABILITIES_TEMPERATURE][0x00]= MultiSTING.strPsudoDeviceCapabilities00;		//"Supported"
	statusSTR[STR_CAPABILITIES_TEMPERATURE][0xFF]= MultiSTING.strPsudoDeviceCapabilitiesFF;		//"Not Support"

	statusSTR[STR_CAPABILITIES_FANSPEEDREPORT][0x00]= MultiSTING.strPsudoDeviceCapabilities00;	//"Supported"
	statusSTR[STR_CAPABILITIES_FANSPEEDREPORT][0xff]= MultiSTING.strPsudoDeviceCapabilitiesFF;	//"Not Support"

	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x00]="JBOD";	//"Port Multiplier";
	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x01]="RAID 0";
	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x02]="RAID 1";
	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x03]="SPAN";
	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x04]="Side Winder";
	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x05]="Split Volumes Mode";
	statusSTR[STR_CAPABILITIES_CURRENTMODE][0x0F]="Virtual HDD";

	/*RAIDSTATUS*/
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x00]="JBOD";
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x01]="RAID 0";
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x02]="RAID 1";
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x03]="SPAN";
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x04]="Side Winder";
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x05]="Split Volumes Mode";
	statusSTR[STR_RAIDSTATUS_CURRENTMODE][0x0F]="Virtual HDD";

	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x00]= MultiSTING.strPsudoDevicRaidStatusRaidStatus00;	//"Good"
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x01]= MultiSTING.strPsudoDevicRaidStatusRaidStatus01;	//"Degraded"	
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x02]= MultiSTING.strPsudoDevicRaidStatusRaidStatus02;	//"Rebuilding"
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x03]= MultiSTING.strPsudoDevicRaidStatusRaidStatus03;	//"Rebuild Failed"
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x04]= MultiSTING.strPsudoDevicRaidStatusRaidStatus04;	//"Data Lost"
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x05]= MultiSTING.strPsudoDevicRaidStatusRaidStatus05;	//"Migrating"
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x06]= MultiSTING.strPsudoDevicRaidStatusRaidStatus06;	//"Verifying"	
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x07]= MultiSTING.strPsudoDevicRaidStatusRaidStatus07;	//"Verify Failed"	
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x08]= MultiSTING.strPsudoDevicRaidStatusRaidStatus08;	//"Rebuilding Paused"	
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x09]= MultiSTING.strPsudoDevicRaidStatusRaidStatus09;	//"Verifying Paused"	
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x10]= MultiSTING.strPsudoDevicRaidStatusRaidStatus10;	//"Not Configured"
	statusSTR[STR_RAIDSTATUS_RAIDSTATUS][0x11]= MultiSTING.strPsudoDevicRaidStatusRaidStatus11;	//"Broken"

	statusSTR[STR_RAIDSTATUS_RAIDMODE][0x00]="JBOD";			//"Port Multiplier";
	statusSTR[STR_RAIDSTATUS_RAIDMODE][0x01]="RAID 0";
	statusSTR[STR_RAIDSTATUS_RAIDMODE][0x02]="RAID 1";
	statusSTR[STR_RAIDSTATUS_RAIDMODE][0x03]="SPAN";
	statusSTR[STR_RAIDSTATUS_RAIDMODE][0x10]="Not Configured";

	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x00]="512 Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x01]="1K  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x02]="2K  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x03]="4K  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x04]="8K  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x05]="16K Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x06]="32K Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x07]="64K Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x08]="128K Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x09]="256K Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x0A]="512K Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x0B]="1M  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x0C]="2M  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x0D]="4M  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x0E]="8M  Bytes";
	statusSTR[STR_RAIDSTATUS_STRIPESIZE][0x0F]="16M Bytes";

	/*DISKSTATUS*/

	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x00]="JBOD";			//"Port Multiplier";
	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x01]="RAID 0";
	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x02]="RAID 1";
	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x03]="SPAN";
	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x04]="Side Winder";
	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x05]="Split Volume Mode";
	statusSTR[STR_DISKSTATUS_CURRENTMODE][0x0F]="Virtual HDD";

	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x00]= MultiSTING.strPsudoDevicRaidStatusPortStatus00;	//"Good"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x01]= MultiSTING.strPsudoDevicRaidStatusPortStatus01;	//"Rebuilding"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x02]= MultiSTING.strPsudoDevicRaidStatusPortStatus02;	//"Empty"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x03]= MultiSTING.strPsudoDevicRaidStatusPortStatus03;	//"Missing"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x04]= MultiSTING.strPsudoDevicRaidStatusPortStatus04;	//"Blank"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x05]= MultiSTING.strPsudoDevicRaidStatusPortStatus05;	//"ID Mismatch"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x06]= MultiSTING.strPsudoDevicRaidStatusPortStatus06;	//"Rejected"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x07]= MultiSTING.strPsudoDevicRaidStatusPortStatus07;	//"Predictive Failure"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x08]= MultiSTING.strPsudoDevicRaidStatusPortStatus08;	//"Failed"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x09]= MultiSTING.strPsudoDevicRaidStatusPortStatus09;	//"Spare"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x0A]= MultiSTING.strPsudoDevicRaidStatusPortStatus0A;	//"Not Participating"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x0B]= MultiSTING.strPsudoDevicRaidStatusPortStatus0B;	//"Verifying"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x0C]= MultiSTING.strPsudoDevicRaidStatusPortStatus0C;	//"Migrating"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x11]= MultiSTING.strPsudoDevicRaidStatusPortStatus11;	//"Rebuilding Pause"
	statusSTR[STR_DISKSTATUS_PORTSTATUS][0x1B]= MultiSTING.strPsudoDevicRaidStatusPortStatus1B;	//"Verifying Pause"


	
	statusSTR[STR_CURRENT_VHDD_REASON][0x00] = MultiSTING.strCurrentVhddReason00;	//"None ATA disk connected";
	statusSTR[STR_CURRENT_VHDD_REASON][0x10] = MultiSTING.strCurrentVhddReason10;	//"RAID 0 crashed";
	statusSTR[STR_CURRENT_VHDD_REASON][0x11] = MultiSTING.strCurrentVhddReason11;	//"SPANcrashed";
	statusSTR[STR_CURRENT_VHDD_REASON][0x12] = MultiSTING.strCurrentVhddReason12;	//"RAID 1 disks mismatched";
	statusSTR[STR_CURRENT_VHDD_REASON][0x13] = MultiSTING.strCurrentVhddReason13;	//"RAID 1 missing";
	statusSTR[STR_CURRENT_VHDD_REASON][0x14] = MultiSTING.strCurrentVhddReason14;	//"";
	statusSTR[STR_CURRENT_VHDD_REASON][0x15] = MultiSTING.strCurrentVhddReason15;	//"";
	statusSTR[STR_CURRENT_VHDD_REASON][0x20] = MultiSTING.strCurrentVhddReason20;	//"RAID type mismatched";
	statusSTR[STR_CURRENT_VHDD_REASON][0x30] = MultiSTING.strCurrentVhddReason30;	//"FORCE VHDD command";

	/**/

}








