#pragma once


// CRaidModeDlg dialog

class CRaidModeDlg : public CDialog
{
	DECLARE_DYNAMIC(CRaidModeDlg)

public:
	CRaidModeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRaidModeDlg();

// Dialog Data
	enum { IDD = IDD_RAIDMODE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
