#include "stdafx.h"
#include "windows.h"
#include "EnumDisk.h"

#include "dbt.h"
#include "outputString.h"
#include "ASM2109.h"
//#include "windows.h"
#include "stdio.h"


extern DEVICE_TABLE g_device[16];

#define     STR_MASS_STORAGE_DRIVER_KEY_NAME						"36FC9E60-C465-11CF-8056-444553540000"

static const GUID GUID_DEVINTERFACE_LIST[] = 
{
	// GUID_DEVINTERFACE_USB_DEVICE
	{ 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } },

	// GUID_DEVINTERFACE_DISK
	{ 0x53f56307, 0xb6bf, 0x11d0, { 0x94, 0xf2, 0x00, 0xa0, 0xc9, 0x1e, 0xfb, 0x8b } },

	// GUID_DEVINTERFACE_HID, 
	{ 0x4D1E55B2, 0xF16F, 0x11CF, { 0x88, 0xCB, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30 } },
			 
	// GUID_NDIS_LAN_CLASS
	{ 0xad498944, 0x762f, 0x11d0, { 0x8d, 0xcb, 0x00, 0xc0, 0x4f, 0xc3, 0x35, 0x8c } },

	// GUID_HDD_DEVICE
	{ 0x4d36e967, 0xe325, 0x11ce, { 0xbf, 0xc1, 0x08, 0x00, 0x2b, 0xe1, 0x03, 0x18}  }

};

ULONG   DebugLevel = 1;
                            // 0 = Suppress All Messages
                            // 1 = Display & Fatal Error Message
                            // 2 = Warning & Debug Messages
                            // 3 = Informational Messages



void DebugPrint( USHORT DebugPrintLevel, PCHAR DebugMessage, ... )
/*++

Routine Description:

    This routine print the given string, if given debug level is <= to the
    current debug level.

Arguments:

    DebugPrintLevel - Debug level of the given message

    DebugMessage    - Message to be printed

Return Value:

  None

--*/
{
/*
    va_list args;

    va_start(args, DebugMessage);

    if (DebugPrintLevel <= DebugLevel) {
        char buffer[256];
        (VOID) vsprintf_s(buffer, DebugMessage, args);
        OutputDebugString( buffer );		
    }

    va_end(args);*/
}

BOOL GetRegistryProperty( HDEVINFO DevInfo, DWORD Index )
/*++

Routine Description:

    This routine enumerates the disk devices using the Setup class interface
    GUID GUID_DEVCLASS_DISKDRIVE. Gets the Device ID from the Registry 
    property.

Arguments:

    DevInfo - Handles to the device information list

    Index   - Device member 

Return Value:

  TRUE / FALSE. This decides whether to continue or not

--*/
{

    SP_DEVINFO_DATA deviceInfoData;
    DWORD           errorCode;
    DWORD           bufferSize = 0;
    DWORD           dataType;
    LPTSTR          buffer = NULL;
    BOOL            status;

    deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
    status = SetupDiEnumDeviceInfo(
                DevInfo,
                Index,
                &deviceInfoData);

    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode == ERROR_NO_MORE_ITEMS ) {
            DebugPrint( 2, "\nNo more devices.\n");
        }
        else {
            DebugPrint( 1, "SetupDiEnumDeviceInfo failed with error: %d\n", errorCode );
        }
        return FALSE;
    }
        
    //
    // We won't know the size of the HardwareID buffer until we call
    // this function. So call it with a null to begin with, and then 
    // use the required buffer size to Alloc the necessary space.
    // Keep calling we have success or an unknown failure.
    //

    status = SetupDiGetDeviceRegistryProperty(
                DevInfo,
                &deviceInfoData,
                SPDRP_HARDWAREID,
                &dataType,
                (PBYTE)buffer,
                bufferSize,
                &bufferSize);

    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode != ERROR_INSUFFICIENT_BUFFER ) {
            if ( errorCode == ERROR_INVALID_DATA ) {
                //
                // May be a Legacy Device with no HardwareID. Continue.
                //
                return TRUE;
            }
            else {
                DebugPrint( 1, "SetupDiGetDeviceInterfaceDetail failed with error: %d\n", errorCode );
                return FALSE;
            }
        }
    }

    //
    // We need to change the buffer size.
    //

    buffer = (LPTSTR)LocalAlloc(LPTR, bufferSize);
    
    status = SetupDiGetDeviceRegistryProperty(
                DevInfo,
                &deviceInfoData,
                SPDRP_HARDWAREID,
                &dataType,
                (PBYTE)buffer,
                bufferSize,
                &bufferSize);

    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode == ERROR_INVALID_DATA ) {
            //
            // May be a Legacy Device with no HardwareID. Continue.
            //
            return TRUE;
        }
        else {
            DebugPrint( 1, "SetupDiGetDeviceInterfaceDetail failed with error: %d\n", errorCode );
            return FALSE;
        }
    }

    DebugPrint( 1, "\n\nDevice ID: %s\n",buffer );
    
    if (buffer) {
        LocalFree(buffer);
    }

    return TRUE;
}

BOOL GetDeviceProperty(HDEVINFO IntDevInfo, DWORD Index )
/*++

Routine Description:

    This routine enumerates the disk devices using the Device interface
    GUID DiskClassGuid. Gets the Adapter & Device property from the port
    driver. Then sends IOCTL through SPTI to get the device Inquiry data.

Arguments:

    IntDevInfo - Handles to the interface device information list

    Index      - Device member 

Return Value:

  TRUE / FALSE. This decides whether to continue or not

--*/
{
    SP_DEVICE_INTERFACE_DATA            interfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA    interfaceDetailData = NULL;
    HANDLE                              hDevice;
    BOOL                                status;
    ULONG                               length = 0,
                                        returned = 0;
    DWORD                               interfaceDetailDataSize,
                                        reqSize,
                                        errorCode;
	CString								strDevicePath;

    interfaceData.cbSize = sizeof (SP_INTERFACE_DEVICE_DATA);

    status = SetupDiEnumDeviceInterfaces ( 
                IntDevInfo,             // Interface Device Info handle
                0,                      // Device Info data
                (LPGUID)&DiskClassGuid, // Interface registered by driver
                Index,                  // Member
                &interfaceData          // Device Interface Data
                );

    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode == ERROR_NO_MORE_ITEMS ) {
            DebugPrint( 2, "No more interfaces\n" );
        }
        else {
            DebugPrint( 1, "SetupDiEnumDeviceInterfaces failed with error: %d\n", errorCode  );
        }
        return FALSE;
    }
        
    //
    // Find out required buffer size, so pass NULL 
    //

    status = SetupDiGetDeviceInterfaceDetail (
                IntDevInfo,         // Interface Device info handle
                &interfaceData,     // Interface data for the event class
                NULL,               // Checking for buffer size
                0,                  // Checking for buffer size
                &reqSize,           // Buffer size required to get the detail data
                NULL                // Checking for buffer size
                );

    //
    // This call returns ERROR_INSUFFICIENT_BUFFER with reqSize 
    // set to the required buffer size. Ignore the above error and
    // pass a bigger buffer to get the detail data
    //

    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode != ERROR_INSUFFICIENT_BUFFER ) {
            DebugPrint( 1, "SetupDiGetDeviceInterfaceDetail failed with error: %d\n", errorCode   );
            return FALSE;
        }
    }

    //
    // Allocate memory to get the interface detail data
    // This contains the devicepath we need to open the device
    //

    interfaceDetailDataSize = reqSize;
    interfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc (interfaceDetailDataSize);
    if ( interfaceDetailData == NULL ) {
        DebugPrint( 1, "Unable to allocate memory to get the interface detail data.\n" );
		free(interfaceDetailData);
        return FALSE;
    }
    interfaceDetailData->cbSize = sizeof (SP_INTERFACE_DEVICE_DETAIL_DATA);

    status = SetupDiGetDeviceInterfaceDetail (
                  IntDevInfo,               // Interface Device info handle
                  &interfaceData,           // Interface data for the event class
                  interfaceDetailData,      // Interface detail data
                  interfaceDetailDataSize,  // Interface detail data size
                  &reqSize,                 // Buffer size required to get the detail data
                  NULL);                    // Interface device info

    if ( status == FALSE ) {
        DebugPrint( 1, "Error in SetupDiGetDeviceInterfaceDetail failed with error: %d\n", GetLastError() );
		free(interfaceDetailData);
        return FALSE;
    }

    //
    // Now we have the device path. Open the device interface
    // to send Pass Through command

    DebugPrint( 2, "Interface: %s\n", interfaceDetailData->DevicePath);
	
	strDevicePath = interfaceDetailData->DevicePath;

    hDevice = CreateFile(
                interfaceDetailData->DevicePath,    // device interface name
                GENERIC_READ | GENERIC_WRITE,       // dwDesiredAccess
                FILE_SHARE_READ | FILE_SHARE_WRITE, // dwShareMode
                NULL,                               // lpSecurityAttributes
                OPEN_EXISTING,                      // dwCreationDistribution
                0,                                  // dwFlagsAndAttributes
                NULL                                // hTemplateFile
                );
                

	IDINFO	IDInfo;
	BOOL	Identify = false;
	UINT64 temp =0 ;
	//ATA COMMAND
	if(!GetAPI_Identify(hDevice, &IDInfo))
	{
		DebugPrint( 1, "Identify faile\n");
		Identify = false;
	}
	else
	{
		UINT8 tempChar;
		for(int i=0; i<40 ;i= i+2 ){
			tempChar = IDInfo.sModelNumber[i];
			IDInfo.sModelNumber[i] = IDInfo.sModelNumber[i+1];
			IDInfo.sModelNumber[i+1] = tempChar;
		}
		IDInfo.sModelNumber[39]= '\0';

		CString tempString;
		tempString = IDInfo.sModelNumber;
		tempString.TrimLeft();
		tempString.TrimRight();
	
		g_device[g_DeviceNum].name = tempString;



		for(int i=0; i<20 ;i= i+2 ){
			tempChar = IDInfo.sSerialNumber[i];
			IDInfo.sSerialNumber[i] = IDInfo.sSerialNumber[i+1];
			IDInfo.sSerialNumber[i+1] = tempChar;
		}
	//	IDInfo.sSerialNumber[19]= '\0';

		tempString.Empty();
		tempString =IDInfo.sSerialNumber;
		tempString.TrimLeft();
		tempString.TrimRight();

		g_device[g_DeviceNum].SN= tempString;

		temp=(UINT64)IDInfo.TotalNumberOfLogicalSectors*512;
		temp = temp/1024;
		temp = temp/1024;
		temp = temp/1024;
		g_device[g_DeviceNum].deviceSize_GB = (UINT16)temp;


		g_device[g_DeviceNum].path= strDevicePath;
		g_device[g_DeviceNum].exist_physical=true;

		if(g_device[g_DeviceNum].name !="" && g_device[g_DeviceNum].SN !=""){
			Identify = true;
		}else{
			Identify = false;
		}
	}

	
	if(	Identify == false){	
		memset(&IDInfo, 0x00, sizeof(IDInfo));
		if(!GetATA_Identify(hDevice, &IDInfo))
		{
			DebugPrint( 1, "Identify faile\n");
			Identify = false;
		}else{



			UINT8 tempChar;
			temp=  IDInfo.TotalNumberOfLogicalSectors * 512;
			temp = temp/1024;
			temp = temp/1024;
			temp = temp/1024;

			g_device[g_DeviceNum].deviceSize_GB = (UINT16)temp;

			for(int i=0; i<40 ;i= i+2 ){
				tempChar = IDInfo.sModelNumber[i];
				IDInfo.sModelNumber[i] = IDInfo.sModelNumber[i+1];
				IDInfo.sModelNumber[i+1] = tempChar;
			}
			for(int i=0; i<20; i=i+2){
				tempChar = IDInfo.sSerialNumber[i];
				IDInfo.sSerialNumber[i] = IDInfo.sSerialNumber[i+1];
				IDInfo.sSerialNumber[i+1] = tempChar;
			}

			IDInfo.sModelNumber[39]		= '\0';

			CString tempString;
			tempString = IDInfo.sModelNumber;
			tempString.TrimLeft();
			tempString.TrimRight();
			g_device[g_DeviceNum].name = tempString;

			tempString.Empty();

			tempString =IDInfo.sSerialNumber;
			tempString.TrimLeft();
			tempString.TrimRight();
			g_device[g_DeviceNum].SN= tempString;


			g_device[g_DeviceNum].path= strDevicePath;
			g_device[g_DeviceNum].exist_physical=true;

			DebugPrint( 1, "Identify success\n");

			if(g_device[g_DeviceNum].name !="" && g_device[g_DeviceNum].SN !=""){
				Identify = true;
			}else{
				Identify = false;
			}
			
		}
	}


	if(	Identify == false){	
		memset(&IDInfo, 0x00, sizeof(IDInfo));
		if(!GetSCSI_ATA_Identify(hDevice, &IDInfo))
		{
			DebugPrint( 1, "Identify faile\n");
			Identify = false;
		}else{

			Index;
			UINT8 tempChar;
			temp=  IDInfo.TotalNumberOfLogicalSectors * 512;
			temp = temp/1024;
			temp = temp/1024;
			temp = temp/1024;

			g_device[g_DeviceNum].deviceSize_GB = (UINT16)temp;

			for(int i=0; i<40 ;i= i+2 ){
				tempChar = IDInfo.sModelNumber[i];
				IDInfo.sModelNumber[i] = IDInfo.sModelNumber[i+1];
				IDInfo.sModelNumber[i+1] = tempChar;
			}
			for(int i=0; i<20; i=i+2){
				tempChar = IDInfo.sSerialNumber[i];
				IDInfo.sSerialNumber[i] = IDInfo.sSerialNumber[i+1];
				IDInfo.sSerialNumber[i+1] = tempChar;
			}

			IDInfo.sModelNumber[39]		= '\0';

			CString tempString;
			tempString = IDInfo.sModelNumber;
			tempString.TrimLeft();
			tempString.TrimRight();
			g_device[g_DeviceNum].name = tempString;

			tempString.Empty();

			tempString =IDInfo.sSerialNumber;
			tempString.TrimLeft();
			tempString.TrimRight();
			g_device[g_DeviceNum].SN= tempString;


			g_device[g_DeviceNum].path= strDevicePath;
			g_device[g_DeviceNum].exist_physical=true;

			if(g_device[g_DeviceNum].name !="" && g_device[g_DeviceNum].SN !=""){
				Identify = true;
			}else{
				Identify = false;
			}
		
		}
	}
	
	if(	Identify == false){	
		memset(&IDInfo, 0x00, sizeof(IDInfo));
		if(!GetSCSI_Identify(hDevice, &IDInfo))
		{
			DebugPrint( 1, "Identify faile\n");
			Identify = false;
		}else{

		//	Index;
		//	UINT8 tempChar;
			temp=  IDInfo.TotalNumberOfLogicalSectors * 512;
			temp = temp/1024;
			temp = temp/1024;
			temp = temp/1024;

			g_device[g_DeviceNum].deviceSize_GB = (UINT16)temp;

	
			IDInfo.sModelNumber[39]		= '\0';

			CString tempString;
			tempString = IDInfo.sModelNumber;
			tempString.TrimLeft();
			tempString.TrimRight();
			g_device[g_DeviceNum].name = tempString;

			tempString.Empty();

			tempString =IDInfo.sSerialNumber;
			tempString.TrimLeft();
			tempString.TrimRight();
			g_device[g_DeviceNum].SN= tempString;


			g_device[g_DeviceNum].path= strDevicePath;
			g_device[g_DeviceNum].exist_physical=true;

			if(g_device[g_DeviceNum].name !="" && g_device[g_DeviceNum].SN !=""){
				Identify = true;
			}else{
				Identify = false;
			}
		
		}
	}
    //
    // Close handle the driver
    //
    if ( !CloseHandle(hDevice) )     {
        DebugPrint( 2, "Failed to close device.\n");
    }
	g_DeviceNum++;

	free(interfaceDetailData);
	return TRUE;
}

VOID
PrintError(ULONG ErrorCode)
/*++

Routine Description:

    Prints formated error message

Arguments:

    ErrorCode   - Error code to print


Return Value:
    
      None
--*/
{
    TCHAR errorBuffer[80];
    ULONG count;

    count = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                    NULL,
                    ErrorCode,
                    0,
                    errorBuffer,
                    sizeof(errorBuffer),
                    NULL
                    );

    if (count != 0) {
        DebugPrint( 1, "%s\n", errorBuffer);
    } else {
        DebugPrint( 1, "Format message failed.  Error: %d\n", GetLastError());
    }
}

VOID
PrintDataBuffer(PUCHAR DataBuffer, ULONG BufferLength)
/*++

Routine Description:

    Prints the formated data buffer

Arguments:

    DataBuffer      - Buffer to be printed

    BufferLength    - Length of the buffer


Return Value:
    
      None
--*/
{
    ULONG cnt;

    DebugPrint( 3, "      00  01  02  03  04  05  06  07   08  09  0A  0B  0C  0D  0E  0F\n");
    DebugPrint( 3, "      ---------------------------------------------------------------\n");
    for (cnt = 0; cnt < BufferLength; cnt++) {
        if ((cnt) % 16 == 0) {
            DebugPrint( 3, " %03X  ",cnt);
            }
        DebugPrint( 3, "%02X  ", DataBuffer[cnt]);
        if ((cnt+1) % 8 == 0) {
            DebugPrint( 3, " ");
        }
        if ((cnt+1) % 16 == 0) {
            DebugPrint( 3, "\n");
        }
    }
    DebugPrint( 3, "\n\n");
}


VOID
PrintStatusResults( BOOL Status, DWORD Returned, PSCSI_PASS_THROUGH_WITH_BUFFERS Psptwb )
/*++

Routine Description:

    Prints the SCSI Inquiry data from the device

Arguments:

    Status      - Status of the DeviceIOControl

    Returned    - Number of bytes returned

    Psptwb      - SCSI pass through structure

Return Value:
    
      None
--*/
{
    ULONG   errorCode;
    USHORT  i, devType;

    DebugPrint( 1, "\nInquiry Data from scsi Pass Through\n");
    DebugPrint( 1, "------------------------------\n");

    if (!Status ) {
        DebugPrint( 1, "Error: %d ", errorCode = GetLastError() );
        PrintError(errorCode);
        return;
    }
    if (Psptwb->Spt.ScsiStatus) {
        PrintSenseInfo(Psptwb);
        return;
    }
    else {
        devType = Psptwb->ucDataBuf[0] & 0x1f ;

        //
        // Our Device Table can handle only 16 devices.
        //
        DebugPrint( 1, "Device Type: %s (0x%X)\n", DeviceType[devType > 0x0F? 0x0F: devType], devType );

        DebugPrint( 1, "Vendor ID  : ");
        for (i = 8; i <= 15; i++) {
            DebugPrint( 1, "%c", Psptwb->ucDataBuf[i] );
        }
        DebugPrint( 1, "\nProduct ID : ");
        for (i = 16; i <= 31; i++) {
            DebugPrint( 1, "%c", Psptwb->ucDataBuf[i] );
        }
        DebugPrint( 1, "\nProduct Rev: ");
        for (i = 32; i <= 35; i++) {
            DebugPrint( 1, "%c", Psptwb->ucDataBuf[i] );
        }
        DebugPrint( 1, "\nVendor Str : ");
        for (i = 36; i <= 55; i++) {
            DebugPrint( 1, "%c", Psptwb->ucDataBuf[i] );
        }
        DebugPrint( 1, "\n\n");
        DebugPrint( 3, "Scsi status: %02Xh, Bytes returned: %Xh, ",
            Psptwb->Spt.ScsiStatus, Returned);
        DebugPrint( 3, "Data buffer length: %Xh\n\n\n",
            Psptwb->Spt.DataTransferLength);
        PrintDataBuffer( Psptwb->ucDataBuf, 192);
        DebugPrint( 1, "\n\n");
    }
}

VOID
PrintSenseInfo(PSCSI_PASS_THROUGH_WITH_BUFFERS Psptwb)
/*++

Routine Description:

    Prints the SCSI status and Sense Info from the device

Arguments:

    Psptwb      - Pass Through buffer that contains the Sense info


Return Value:
    
      None
--*/
{
    int i;

    DebugPrint( 1, "Scsi status: %02Xh\n\n", Psptwb->Spt.ScsiStatus);
    if (Psptwb->Spt.SenseInfoLength == 0) {
        return;
    }
    DebugPrint( 3, "Sense Info -- consult SCSI spec for details\n");
    DebugPrint( 3, "-------------------------------------------------------------\n");
    for (i=0; i < Psptwb->Spt.SenseInfoLength; i++) {
        DebugPrint( 3, "%02X ", Psptwb->ucSenseBuf[i]);
    }
    DebugPrint( 1, "\n\n");
}

void RegisterDeviceNotification(HWND hWnd)
{

	DEV_BROADCAST_DEVICEINTERFACE	NotificationFilter;
	DWORD	dwErrorCode = 0;
	int		iGUIDIdx = 0;

	GUID guidRet;
	CString strGUID = STR_MASS_STORAGE_DRIVER_KEY_NAME;



	
	if( RPC_S_OK == UuidFromString((unsigned char*)strGUID.GetBuffer(),&guidRet) )
	{
	}
	else
	{
		::ZeroMemory(&guidRet, sizeof(guidRet) );
		dwErrorCode = ::GetLastError();
		DebugPrint( 1, "vCStringToGUID: UuidFromString Fail , dwErrorCode %d", dwErrorCode);
	}
	try
	{
		::ZeroMemory( &NotificationFilter, sizeof(NotificationFilter) );

		NotificationFilter.dbcc_classguid = guidRet;
	
		NotificationFilter.dbcc_size		= sizeof(DEV_BROADCAST_DEVICEINTERFACE);
		NotificationFilter.dbcc_devicetype	= DBT_DEVTYP_DEVICEINTERFACE;

		NotificationFilter.dbcc_classguid = GUID_DEVINTERFACE_LIST[5];


		g_hDevNotify = ::RegisterDeviceNotification(hWnd, &NotificationFilter, DEVICE_NOTIFY_WINDOW_HANDLE);
		if( !g_hDevNotify)
		{
				dwErrorCode = ::GetLastError();
				DebugPrint(1, "vRegisterDeviceNotification: RegisterDeviceNotification Fail:%d ", dwErrorCode );				
		}
	}
	catch(...)
	{
		dwErrorCode = ::GetLastError();
		DebugPrint( 1, "vRegisterDeviceNotification , dwErrorCode %d", dwErrorCode);
	}
}

void UnRegisterDeviceNotification()
{
	DWORD	dwErrorCode = 0;
	int		iGUIDIdx = 0;
	try
	{
		if( ::UnregisterDeviceNotification(g_hDevNotify) )
		{
		}
		else
		{
				dwErrorCode = ::GetLastError();
				DebugPrint(1, "vUnRegisterDeviceNotification: UnregisterDeviceNotification Fail , dwErrorCode %d",  dwErrorCode);
		}

	}
	catch(...)
	{
		dwErrorCode = ::GetLastError();
		DebugPrint( 1,  "vUnRegisterDeviceNotification , dwErrorCode %d",  dwErrorCode);
	}
}

void EnumDevice()
{
    HDEVINFO        hDevInfo, hIntDevInfo;
    DWORD           index;
    BOOL            status;



    hDevInfo = SetupDiGetClassDevs(
                    (LPGUID) &GUID_DEVCLASS_DISKDRIVE,
                    NULL,
                    NULL, 
                    DIGCF_PRESENT  ); // All devices present on system
    if (hDevInfo == INVALID_HANDLE_VALUE)
    {
        DebugPrint(1, "SetupDiGetClassDevs failed with error: %d\n", GetLastError()   );  
		return;
    }	

    hIntDevInfo = SetupDiGetClassDevs (
                 (LPGUID)&DiskClassGuid,
                 NULL,                                   // Enumerator
                 NULL,                                   // Parent Window
                 (DIGCF_PRESENT | DIGCF_INTERFACEDEVICE  // Only Devices present & Interface class
                 ));

    if( hDevInfo == INVALID_HANDLE_VALUE ) {
        DebugPrint( 1, "SetupDiGetClassDevs failed with error: %d\n", GetLastError() );
		return;
    }

    index = 0;

    while (TRUE) 
    {
        DebugPrint(1, "Properties for Device %d", index+1);

        status = GetRegistryProperty( hDevInfo, index );
        if ( status == FALSE ) {
            break;
        }

        status = GetDeviceProperty( hIntDevInfo, index );
        if ( status == FALSE ) {
            break;
        }
        index++;
    }

    DebugPrint( 1, "\r\n ***  End of Device List  *** \r\n");
    SetupDiDestroyDeviceInfoList(hDevInfo);
    SetupDiDestroyDeviceInfoList(hIntDevInfo);

}

