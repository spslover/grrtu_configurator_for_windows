#pragma once

#ifndef _ASM2109_H
#define _ASM2109_H


#pragma pack(1)             //Byte aligment for data structure 


typedef unsigned int    UINT;
typedef UINT            UINTN;

//Header field LBA definitions
#define HEADER_NUMBER                   04          //Header LBA number is fixed 4
#define HEADERLBA0                      0x0043      //0x43(67) First Header LBA
#define HEADERLBA1                      0x0071      //0x71(113)Second Header LBA
#define HEADERLBA2                      0x00A7      //0xA7(167) Third Header LBA
#define HEADERLBA3                      0x00DF      //0xDF(223) Fourth Header LBA

//Type field LBA definitions
#define TYPE_NUMBER                     04          //Type LBA number is fixed 4
#define TYPE_LBA_BASE                   0x100       //Type LBA starts from 0x100
#define WITHOUT_DATA_FIELD              0x00        //First type 0x00 indicates without Data Field
#define WITH_DATA_FIELD                 0x01        //First type 0x11 indicates with Data Field
#define GET_COMMAND                     0x04        //Second type 0x04 indicates get command from FW
#define SET_COMMAND                     0x05        //Second type 0x05 indicates set command to FW
//Cascade layer LBA
#define CASCADE_LAYER0                  0x00        //Bit 7:4 Cascade layer 0
#define CASCADE_LAYER1                  0x10        //Bit 7:4 Cascade layer 1
#define CASCADE_LAYER2                  0x20        //Bit 7:4 Cascade layer 2
#define CASCADE_LAYER_NA                0xEF        //Bit 7:4 Cascade layer is not available
#define MAXIMUM_CASCADE_LAYER           02          //Bit 7:4 Cascade layer only 0, 1, 2

//Cascade port LBA
#define CASCADE_L2P_PORT0               0x00        //Bit 7:4 Cascade layer 2 device port 0
#define CASCADE_L2P_PORT1               0x10        //Bit 7:4 Cascade layer 2 device port 1
#define CASCADE_L2P_PORT2               0x20        //Bit 7:4 Cascade layer 2 device port 2
#define CASCADE_L2P_PORT3               0x30        //Bit 7:4 Cascade layer 2 device port 3
#define CASCADE_L2P_PORT4               0x40        //Bit 7:4 Cascade layer 2 device port 4
#define CASCADE_L1P_PORT0               0x00        //Bit 3:0 Cascade layer 1 device port 0
#define CASCADE_L1P_PORT1               0x01        //Bit 3:0 Cascade layer 1 device port 1
#define CASCADE_L1P_PORT2               0x02        //Bit 3:0 Cascade layer 1 device port 2
#define CASCADE_L1P_PORT3               0x03        //Bit 3:0 Cascade layer 1 device port 3
#define CASCADE_L1P_PORT4               0x04        //Bit 3:0 Cascade layer 1 device port 4
#define CASCADE_PORT_NA                 0xFF        //Cascade port is not available
#define MAXIMUM_CASCADE_PORT            04          //Cascade port

//Command field LBA definitions
#define COMMAND_NUMBER                  04          //Command LBA number is fixed 4
#define COMMAND_LBA_BASE                0x200       //Command LBA starts from 0x200
//Command LBA
//Command Table
#define IDENTIFY                        0x10        //Distinguish ASM2109
#define READ_FLASH_ROM                  0x20        //Read data from flash ROM
#define ERASE_FLASH_ROM_SECTOR          0x23        //Erase a specific address range of flash ROM
#define FLASH_ROM_SECTOR				4096        //Erase sector will erase 4KB
#define WRITE_FLASH_ROM                 0x25        //Program flash ROM
#define RESET                           0xF0        //Reset ASM2109
#define GET_REGISTER                    0x50        //Get ASM2109 register value
#define SET_REGISTER                    0x51        //Set ASM2109 register value
#define BIST_START                      0x60        //Start ASM2109 built in self test
#define BIST_STOP                       0x61        //Stop ASM2109 built in self test
#define GET_CAPABILITIES                0x11        //Get ASM2109 capabilities
#define MODE_CHANGE                     0xB0        //Chang the mode of ASM2109
#define GET_RAID_STATUS                 0x15        //Get RAID information and status
#define GET_DISKS_STATUS                0x17        //Get disks information and status
#define REBUILD                         0x30        //Rebuild operation for RAID 1, SideWinder
#define _VERIFY_                        0x35        //Verify operation for RAID 1, SideWinder
#define GET_ENCLOSURE_STATUS            0x13        //Get enclosure status
#define GET_TOPOLOGY                    0x19        //Get cascade mode topology

//Subcommand LBA
#define SUBCMD_NA                       0xCF        //Subcommand is not available
//Sequence number MSB
#define SEQHI_NA                        0xDF        //Sequence number MSB is not available
//Sequence number MSB
#define SEQLO_NA                        0xEF        //Sequence number LSB is not available

//SubCommand for READ_FLASH_ROM, ERASE_FLASH_ROM_SECTOR, WRITE_FLASH_ROM
#define OFFSET_ADDRESS                  0x00
#define ABSOLUTE_ADDRESS                0x01

#define MAXIMUM_SEQUENCE_NUMBER         0xFFFF      //Maximum sequence number

//Data field LBA definitions
#define DATA_NUMBER                     128         //Data LBA number is fixed 128
#define DATA_LBA_BASE                   0x300       //Data LBA starts from 0x300

//CRC field LBA definitions
#define CRC_NUMBER                      04          //CRC LBA number is fixed 4
#define CRC_LBA_BASE                    0x400       //CRC LBA starts from 0x400

//ACK LBA definitions
#define ACK_NUMBER                      01          //CRC LBA number is fixed 1
#define ACKLBA                          0x0011      //0x11(17) ACK LBA number

//Miscellaneous Definitions
#define HEAD_SIGNATURE              "__ASMEDIA2109A__"          //ACK Data head signature
#define HEAD_SIGNATURE_LENGTH       16                          //Head signature length = 16 bytes
#define TAIL_SIGNATURE              "_PORTMULTIPLIER_"          //ACK Data tail signature
#define TAIL_SIGNATURE_LENGTH       16                          //Tail signature length = 16 bytes
#define MAX_SECTOR_LENGTH           4096                        //Maximum sector length = 4k bytes

// Disk Read Write Buffer Command
#define READ_BUFFER					0xE4                       //Read Buffer Command
#define WRITE_BUFFER				0xE8                       //Write Buffer Command
#define NON_DATA					false
#define HAVE_DATA					true
#define READ						false
#define WRITE						true

//Herder Field structure: 8 bytes
typedef struct _INTERFACE_HEADER {
    UINT16  HeaderLBA0;                             //Little endian of Header first LBA
    UINT16  HeaderLBA1;                             //Little endian of Header second LBA
    UINT16  HeaderLBA2;                             //Little endian of Header third LBA
    UINT16  HeaderLBA3;                             //Little endian of Header fourth LBA
} IFHEADER, *PIFHEADER;

//Type Field structure: 4 bytes
typedef struct _INTERFACE_TYPE {
    UINT8       DataField;                          //with/without Data Field
    UINT8       CommandType;                        //Get/Set Command
    UINT8       CascadeLayer;                       //for which cascade layer
    UINT8       CascadeLogicPort;                   //for which cascade logic port
}IFTYPE_2, *PIFTYPE;

//Command Field structure: 4 bytes
typedef struct _INTERFACE_COMMAND {
    UINT8       Command;                            //Command
    UINT8       Subcommand;                         //Subcommand
    union{
        struct{
            UINT8   SequenceNumberMSB;              //Sequence number MSB
            UINT8   SequenceNumberLSB;              //Sequence number LSB
        };
        UINT16  SequenceNumber;                     //Big endian sequence number
    };
} IFCOMMAND, *PIFCOMMAND;

//Data Field structure: 128 bytes
typedef struct _INTERFACE_DATA {
    UINT8   Data[128];                              //128 bytes data
} IFDATA, *PIFDATA;

//CRC Field structure: 4 bytes
typedef struct _INTERFACE_CRC {
    union {
        struct {
            UINT8   Crc31_24;                       //CRC[31�K24]
            UINT8   Crc23_16;                       //CRC[23�K16]
            UINT8   Crc15_8;                        //CRC[15�K8]
            UINT8   Crc7_0;                         //CRC[7�K0]
        };
        UINT32  Crc;                                //Big endian of CRC
    };
} IFCRC, *PIFCRC;

//Separate non-data and data Packet to sync. hardware design
typedef struct _INTERFACE_DATA_PACKET {
    IFHEADER        Header;                         // 8 bytes
    IFTYPE_2          Type;                           // 4 bytes
    IFCOMMAND       Command;                        // 4 bytes
    union{
        struct{
            IFDATA          Data;                   // 128 bytes
            IFCRC           DataCrc;                // 4 bytes
        };
        struct{
            IFCRC           NonDataCrc;             // 4 bytes
            UINT8           Reserved[128];          // 128 bytes
        };
    };
} IFPACKET, *PIFPACKET;


#define CRC_LENGTH_WITH_DATA            sizeof(IFHEADER)+sizeof(IFTYPE)+sizeof(IFCOMMAND)+sizeof(IFDATA)
#define CRC_LENGTH_WITHOUT_DATA         sizeof(IFHEADER)+sizeof(IFTYPE)+sizeof(IFCOMMAND)


//Acknowledge Data structure: 512 bytes
typedef struct _INTERFACE_ACK_DATA {
    UINT8           HeadSignature[16];              //Head Signature 16 bytes
    UINT8           Rev0[32];                       //Reserved 32 bytes, shall be zero
    UINT8           Status;                         //Acknowledge status 1 byte
    UINT8           Rev1[15];                       //Reserved 15 bytes, shall be zero. For 16 bytes alignment
    IFPACKET        Packet;                         //Packet 148 bytes
    UINT8           AckBuffer[284];                 //Acknowledge Buffer depends on Command 284 bytes
    UINT8           TailSignature[16];              //Tail Signature 16 bytes
} IFACKDATA, *PIFACKDATA;

//ACK Status Definitions
#define CMD_RECEIVED                0x00            //Command received
#define CMD_INPROCESS               0x10            //Command in process
#define CMD_COMPLETE                0x20            //Command complete without error
#define CMD_BLOCKED                 0xC0            //Command is blocked and can't be executed
#define CMD_MISMATCH                0xD0            //Subcommand mismatched
#define CMD_CRC_ERROR               0xE0            //CRC error
#define CMD_UNSUPPORTED             0xF0            //Command unsupported
#define CMD_FAILURE                 0xFF            //Command failure

//Interface status
typedef enum _INTERFACE_STATUS {
    IFSuccess       = 0x00,                         //No error
    IFReceived      = CMD_RECEIVED,                 //Command received
    IFInprocess     = CMD_INPROCESS,                //Command in process
    IFComplete      = CMD_COMPLETE,                 //Command complete without error
    IFBlock         = CMD_BLOCKED,                  //Command is blocked and can't be executed
    IFMismatch      = CMD_MISMATCH,                 //Command mismatched
    IFCrcError      = CMD_CRC_ERROR,                //CRC error
    IFUnsupported   = CMD_UNSUPPORTED,              //Command unsupported
    IFFailure       = CMD_FAILURE,                  //Command failure
    IFReadHDDFail   = 0xF1,                         //Read HDD fail or wrong HDD handle
    IFAckTimeout    = 0xF2,                         //Read Acknowledge data timeout
    IFUnknownError  = 0xF3,                         //Unknown Error
    IFInvalidStatus = 0xF4,                         //Invalid Status
    IFMemoryInsufficient    = 0xF5,                 //Memory Insufficient
    IFInvalidParameter      = 0xF6,                 //Invalid parameters
	IFNotVirtualHDD			= 0xEE,					//Not Virtual HDD
} IFSTATUS;

//Command Packet structure: used by Application
typedef struct _COMMAND_PACKET {
    UINTN           DriveHandle;                    //HDD handle
    IFPACKET        Packet;                         //Packet sent to Firmware
    BOOL            DataField;                      //With/Without Data Field
    UINT32         pLBA[512];                           //Physical LBA sent to disk
    UINTN           LBAElement;                     //How many elements of pLBA to be sent
    UINT8          pData[512];                          //Point to a data buffer to read a sector, store ACKDATA if Status = IFSuccess
    UINT32          AckLBA;                         //Acknowledge Physical LBA
    IFSTATUS        AckStatus;                      //Status returned by Read Acknowledge
} COMMANDPACKET, *PCOMMANDPACKET;

#define IGNORE_CASCADE      0xFF                    //Ignore parameter for Cascade layer, port

#endif  //_ASM2109_H

#define FIRST_HDD_INT13_NUMBER      0x80

typedef struct Disk_Address_Packet
{
    BYTE    Packet_Size;                //Packet size in bytes
    BYTE    Reserved_1;                 //Reserved, shall be 0
    BYTE    Block_Count;                //Number of blocks to transfer
    BYTE    Reserved_2;                 //Reserved, shall be 0
    WORD    Buffer_Offset;              //Address offset of transfer buffer
    WORD    Buffer_Segment;             //Address segment of transfer buffer
    DWORD   Lo_Block_Number;            //Low DWORD of Starting logical block address
    DWORD   Hi_Block_Number;            //High DWORD of Starting logical block address
    DWORD   _64_Bit_Buf_Add_Lo;         //64 bit flat address of the transfer buffer
    DWORD   _64_Bit_Buf_Add_Hi;         //64 bit flat address of the transfer buffer
    DWORD   Total_Block_Number;         //Total number of block to transfer when the data at offset 2 is set to FFh
    DWORD   Reserved_3;                 //Reserved, shall be 0
}DAP;


//BIST command definition
//SubCommand
#define BIST_DEVICE_PORT0           0x01            //Test device port 0
#define BIST_DEVICE_PORT1           0x02            //Test device port 1
#define BIST_DEVICE_PORT2           0x04            //Test device port 2
#define BIST_DEVICE_PORT3           0x08            //Test device port 3
#define BIST_DEVICE_PORT4           0x10            //Test device port 4
//Sequence High
#define BIST_SATA_GEN1              0x01            //Test SATA generation 1
#define BIST_SATA_GEN2              0x02            //Test SATA generation 2
#define BIST_SATA_GEN3              0x04            //Test SATA generation 3
//Test results
typedef struct _BIST_RESULTS {
    UINT8   Port0SATA1;                             //Device port 0 SATA Gen 1 test result
    UINT8   Port0SATA2;                             //Device port 0 SATA Gen 2 test result
    UINT8   Port0SATA3;                             //Device port 0 SATA Gen 3 test result
    UINT8   Port1SATA1;                             //Device port 1 SATA Gen 1 test result
    UINT8   Port1SATA2;                             //Device port 1 SATA Gen 2 test result
    UINT8   Port1SATA3;                             //Device port 1 SATA Gen 3 test result
    UINT8   Port2SATA1;                             //Device port 2 SATA Gen 1 test result
    UINT8   Port2SATA2;                             //Device port 2 SATA Gen 2 test result
    UINT8   Port2SATA3;                             //Device port 2 SATA Gen 3 test result
    UINT8   Port3SATA1;                             //Device port 3 SATA Gen 1 test result
    UINT8   Port3SATA2;                             //Device port 3 SATA Gen 2 test result
    UINT8   Port3SATA3;                             //Device port 3 SATA Gen 3 test result
    UINT8   Port4SATA1;                             //Device port 4 SATA Gen 1 test result
    UINT8   Port4SATA2;                             //Device port 4 SATA Gen 2 test result
    UINT8   Port4SATA3;                             //Device port 4 SATA Gen 3 test result
} BISTRESULTS, *PBISTRESULTS;

#define BIST_RESULTS_LENGTH         sizeof(BISTRESULTS)
#define BIST_SUCCESS                0x00
#define BIST_FAIL                   0x81
#define BIST_NOT_TEST               0xFF

//GET CAPABILITIES command definition
typedef struct _CAPABILITIES_STRUCTURE {
    //Chip and Firmware Information
    UINT8   ChipID;                                 //ASM109x Chip Identifier
    UINT8   DevicePort;                             //Numbers of device port
    UINT16  VendorID;                               //Vendor Identifier (little endian)
    UINT16  DeviceID;                               //Device Identifier (little endian)
    UINT16  Rev0;                                   //Reserved
	UINT64  FirmwareVersion;						//12 ASCII characters
    UINT64  SerialNumber;                           //Rondom Serial Number generated by firmware
    UINT32  Rev1;                                   //Reserved
	UINT8   CurrentImage;                           //Current running firmware image
    UINT8   DualImage;                              //Dual image supported
    UINT8   BIST;                                   //BIST Supported
    UINT8   Cascade;                                //Port Multiplier cascade mode supported
    UINT8   Rev2[4];                                //Reserved
    //RAID information
    UINT8   CurrentMode;                            //Current running mode
    UINT8   SWModeChange;                           //Software utility mode change supported
    UINT16  ModeSupported;                          //RAID mode supported
    UINT8   FunctionSupported;                      //RAID function supported
    UINT8   Rev3[3];                                //Reserved 3 bytes
    //Enclosure information
    UINT8   TemperatureReport;                      //Enclosure temperature report supported
    UINT8   FanSpeedReport;                         //Enclosure fan speed report supported
    UINT8   FanSpeedControl;                        //Enclosure fan speed control supported
    UINT8   Rev4;                                   //Reserved 1 bytes
	UINT8	PMaware;								//PM Aware
	UINT8	RaidSetName[15];
} CAPABILITIES, *PCAPABILITIES;

#define CAPABILITIES_STRUCTURE_LENGTH   sizeof(CAPABILITIES)

#define SUPPORTED                   0x00
#define NOT_SUPPORTED               0xFF
//Chip and Firmware Information
#define ROM_CODE                    0x00
#define RAM_CODE_IMG1_64KB          0x01
#define RAM_CODE_IMG2_64KB          0x02
#define RAM_CODE_IMG1_128KB         0x81
#define RAM_CODE_IMG2_128KB         0x82
//RAID information
#define CURRENT_MODE_JBOD			0x00            //Current running SPANmode (Port Multiplier mode)
#define CURRENT_MODE_RAID0          0x01            //Current running RAID 0 mode
#define CURRENT_MODE_RAID1          0x02            //Current running RAID 1 mode
#define CURRENT_MODE_SPAN			0x03            //Current running SPAN mode
#define CURRENT_MODE_SIDEWINDER     0x04            //Current running SideWinder mode
#define CURRENT_MODE_MULTIVOLUME    0x05            //Current running Multi-Volume mode
#define CURRENT_MODE_VIRTUALHDD     0x0F            //Current running Virtual HDD mode
#define JBOD_SUPPORTED              0x01            //SPANmode (Port Multiplier mode) supported
#define RAID0_SUPPORTED             0x02            //RAID 0 mode supported
#define RAID1_SUPPORTED             0x04            //RAID 1 mode supported
#define SPAN_SUPPORTED              0x08            //Span mode supported
#define SIDEWINDER_SUPPORTED        0x10            //SideWinder mode supported
#define SPLIT_VOLUME_SUPPORTED      0x20            //Split Volume mode supported
#define AUTO_REBULID_SUPPORTED      0x01            //Auto Rebuild supported
#define VERIFY_SUPPORTED            0x02            //Verify supported
#define REBUILD_PAUSE_SUPPORTED     0x04            //Rebuild pause supported
#define VERIFY_PAUSE_SUPPORTED      0x08            //Verify pause supported
#define AUTO_SCRUBBING_SUPPORTED    0x10            //Auto background scrubbing supported


//RAID Mode definotion
#define JBOD_MODE		            0x00            //JBOD_MODE or Port Multiplier
#define RAID0_MODE                  0x01            //RAID 0
#define RAID1_MODE                  0x02            //RAID 1
#define SPAN_MODE					0x03            //SPAN
#define SIDEWINDER_MODE             0x04            //SideWinder
#define SPLIT_MODE                  0x05            //Split Volumes Mode
#define VHDD_NONE_DEVICE            0x0F            //Virtual HDD mode - None device
#define VHDD_NOT_CONFIGURED         0x10            //Virtual HDD mode - Not configured
#define VOLUME_NOT_CONFIGURED       0x10            //Volume Not Configured (include Conflicting Arrays)

//Mode CHANGE command definition
//Volume Descriptor - 28 bytes
typedef struct _VOLUME_DESCRIPTOR {
    UINT8   Mode;                                   //RAID mode to be changed
    UINT8   Type;                                   //Volume type
    UINT8	createMethod;                                //Reserved 2 bytes
	UINT8	Rev0;
    UINT64  StartLBA;                               //Start LBA, big endian
    UINT64  Blocks;                                 //Number of blocks (sectors) of this volume
    UINT8   Port0Order;                             //Order of device port0
    UINT8   Port1Order;                             //Order of device port1
    UINT8   Port2Order;                             //Order of device port2
    UINT8   Port3Order;                             //Order of device port3
    UINT8   Port4Order;                             //Order of device port4
    UINT8   Rev1[3];                                //Reserved 3 bytes
} VOLUMEDESCRIPTOR, *PVOLUMEDESCRIPTOR;

#define VOLUME_DESCRIPTOR_LENGTH    sizeof(VOLUMEDESCRIPTOR)

//RAID Configuration Parameter Data - 128 bytes
typedef struct _RAID_PARAMETER {
    UINT8               Rev0;                     //Create RAID method
    UINT8               Disks;                      //Number of disks for RAID mode
    UINT8               Volumes;                    //Number of Volumes
    UINT8               Rev1[5];                    //Reserved 5 bytes
    union{
		struct{
			VOLUMEDESCRIPTOR    VolumeDescriptor[4];        //Volume Descriptor, max. 4 volumes
		};
		struct{
            VOLUMEDESCRIPTOR    VolumeDescriptor_HGST[3];     //Volume Descriptor, max. 3 volumes              
            UINT8				RaidSetName[28];
        };
	};
    UINT8               Rev2[8];                    //Reserved 8 bytes
} RAIDPARAMETER, *PRAIDPARAMETER;

#define RAID_PARAMETER_LENGTH       sizeof(RAIDPARAMETER)

//Create RAID method
//RAID 1, SideWinder creation method
#define RAID1_QUICK                 0x80            //Quick creation for RAID 1, SideWinder
#define RAID1_FULL                  0x81            //Full creation for RAID 1, SideWinder
//RAID 0 creation method
#define RAID0_NOTCARE               0x80            //Don't care the original data
#define RAID0_MIGRATE               0x81            //Migrate the 1st order device port

//Volume type
#define HDD_VOLUME                  0x80            //HDD volume
#define VCD_VOLUME                  0x81            //VCD volume

//Special blocks number
#define REMAINING_CAPACITY          0xFFFFFFFFFFFFFFFFL     //Remaining capacity


//GET RAID STATUS command definition
//Volume Status - 16 bytes
typedef struct _VOLUME_STATUS {
    UINT8   Status;                                         //RAID status
    UINT8   Mode;                                           //RAID mode
    UINT8   StripeSize;                                     //Stripe size for RAID 0 or Virtual Stripe size for RAID 1
	UINT8	Rev0[3];										//Reserved 3 bytes.
    UINT16  BytesPerBlock;                                  //Bytes per logic block (sector), big endian
    UINT64  TotalBlocks;                                    //Total logic blocks (sectors) of volume, big endian
	UINT64	StartLBA;										//Start absolute LBA;
	UINT64	EndLBA;											//End absolute LBA;
	UINT64	TagLBA;											
} VOLUMESTATUS, *PVOLUMESTATUS;

#define VOLUME_STATUS_LENGTH        sizeof(VOLUMESTATUS)

//RAID Status - 64 bytes
typedef struct _RAID_STATUS {
    UINT8               Mode;                               //Current Running Mode
    UINT8               Disks;                              //Number of Disks for RAID mode
    UINT8               Volumes;                            //Number of Volumes
    UINT8               VHDDReason;                               //Reserved 1 bytes
    VOLUMESTATUS        VolumeStatus[4];                    //Volume status, max. 4 volumes
} RAIDSTATUS, *PRAIDSTATUS;

#define RAID_STATUS_LENGTH          sizeof(RAIDSTATUS)


//RAID Status
#define RAID_GOOD                   0x00                    //Good - Volume set is healthy. The user's data is accessible.
#define RAID_DEGRADED               0x01                    //Degraded - Volume set has seen a problem but the user's data is still accessible by using the array's redundancy data.
#define RAID_REBUILDING             0x02                    //Rebuilding - Volume set is currently being rebuilt. The user's data is accessible.
#define RAID_REBUILD_FAILED         0x03                    //Rebuild Failed - The target of the rebuild operation failed, but the user's data is still available. Another rebuild can be attempted.
#define RAID_DATA_LOST              0x04                    //Data Lost - Volume set has experience enough errors that the user's data has been corrupted, but the user's data is still accessible
#define RAID_MIGRATING              0x05                    //Migrating - Volume set is currently being migrated. The user's data is not accessible. If the migration result is failed, the RAID status becomes Broken.
#define RAID_VERIFYING              0x06                    //Verifying - Volume set is currently being verified.
#define RAID_VERIFY_FAILED          0x07                    //Verify Failed - The target of the verify operation failed, but the user's data is still available. Another rebuild or verify can be attempted.
#define RAID_REBUILDING_PAUSED      0x08                    //Rebuilding Paused - The target of the rebuild operation was paused. The user's data is accessible.
#define RAID_VERIFYING_PAUSED       0x09                    //Verifying Paused - The target of the verify operation was paused. The user's data is accessible.
#define RAID_NOT_CONFIGURED         0x10                    //Not Configured - The array has not been created. The RAID mode can't be determined by firmware (include Conflicting Arrays).
#define RAID_BROKEN                 0x11                    //Broken - The user's data is no longer accessible.

//Stripe Size
#define STRIPE_SIZE_512B            0x00                    //512 bytes
#define STRIPE_SIZE_1KB             0x01                    //1K bytes
#define STRIPE_SIZE_2KB             0x02                    //2K bytes
#define STRIPE_SIZE_4KB             0x03                    //4K bytes
#define STRIPE_SIZE_8KB             0x04                    //8K bytes
#define STRIPE_SIZE_16KB            0x05                    //16K bytes
#define STRIPE_SIZE_32KB            0x06                    //32K bytes
#define STRIPE_SIZE_64KB            0x07                    //64K bytes
#define STRIPE_SIZE_128KB           0x08                    //128K bytes
#define STRIPE_SIZE_256KB           0x09                    //256K bytes
#define STRIPE_SIZE_512KB           0x0A                    //512K bytes
#define STRIPE_SIZE_1MB             0x0B                    //1M bytes
#define STRIPE_SIZE_2MB             0x0C                    //2M bytes
#define STRIPE_SIZE_4MB             0x0D                    //4M bytes
#define STRIPE_SIZE_8MB             0x0E                    //8M bytes
#define STRIPE_SIZE_16MB            0x0F                    //16M bytes


//GET DISKS STATUS command definition
//Device Port Status - 76 bytes
typedef struct _PORT_STATUS {
    UINT8   Status;                                         //Port status
    UINT8   Order;                                          //Port order
    UINT8   Rev0[2];                                        //Reserved 2 bytes
    UINT8   ModelNumber[40];                                //Device Model Number, 40 bytes ATA string
    UINT8   SerialNumber[20];                               //Device Serial Number, 20 bytes ATA string
    UINT8   DiskType;                                       //Disk type, HDD or SSD
    UINT8   Rev1[1];                                        //Reserved 1 bytes
    UINT16  BytesPerBlock;                                  //Bytes per logic block (sector), big endian
    UINT64  TotalBlocks;                                    //Total logic blocks (sectors) of volume, big endian
} PORTSTATUS, *PPORTSTATUS;

#define HDD_TPYE                    0x00                    //Disk type: HDD
#define SSD_TPYE                    0x01                    //Disk type: SSD
#define ATA_TPYE					0x02
#define ATAPI_TPYE					0x03
#define UNKNOW						0x04
#define CANNOT_DETECT				0x05

#define PORT_STATUS_LENGTH   sizeof(PORTSTATUS)

//Disks Status - 364 bytes
typedef struct _DISK_STATUS {
    UINT8           Mode;                                   //Current running mode
    UINT8           Ports;                                  //Number of Device Ports
    UINT8           Disks;                                  //Number of Disks connected
    UINT8           Rev0;                                   //Reserved 1 bytes
    PORTSTATUS      PortStatus[5];                          //Device Port status, max. 5 ports
} DISKSTATUS, *PDISKSTATUS;

typedef struct REG_PORT_STATUS {
    UINT8           regPort0;									//Status of port0 
    UINT8           regPort1;									//Status of port1 
    UINT8           regPort2;									//Status of port2 
} REGPORTSTATUS, *PREGPORTSTATUS;


#define DISK_STATUS_LENGTH          sizeof(DISKSTATUS)

//Port Status
#define PORT_GOOD                   0x00                    //Good - Device Port contains a healthy disk that is part of the volume set.
#define PORT_REBUILDING             0x01                    //Rebuilding - Device Port contains the target disk of the rebuild operation and that rebuild operation is still in progress.
#define PORT_EMPTY                  0x02                    //Empty - There is no disk plugged into this Device Port, but no disk was expected
#define PORT_MISSING                0x03                    //Missing - The expected disk is not plugged into this Device Port
#define PORT_BLANK                  0x04                    //Blank - Device Port contains a disk without any valid SuperBlock (metadata)
#define PORT_ID_MISMATCH            0x05                    //ID Mismatch - Device Port contains a disk with metadata, but the disk's serial number or other identification doesn't match the metadata.
#define PORT_REJECTED               0x06                    //Rejected - Device Port contains a disk that is not supported (i.e. not on whitelist)
#define PORT_PREDICTIVE_FAILURE     0x07                    //Predictive Failure - Device Port contains a disk reporting a SMART predictive failure
#define PORT_FAILED                 0x08                    //Failed - Device Port contains a failed disk
#define PORT_SPARE                  0x09                    //Spare - Device Port contains a spare disk
#define PORT_NOT_PARTICIPATING      0x0A                    //Not Participating - Device Port contents are irrelevant to this volume set
#define PORT_VERIFYING              0x0B                    //Verifying - Device Port contains the target disk of the verify operation and that verify operation is still in progress.
#define PORT_MIGRATING              0x0C                    //Migrating - Device Port contains the disk of the migration operation and that migration operation is still in progress.
#define PORT_REBUILDING_PAUSED      0x11                    //Rebuilding Pause - Device Port contains the target disk of the rebuild operation but that rebuild operation is paused.
#define PORT_VERIFYING_PAUSED       0x1B                    //Verifying Pause - Device Port contains the target disk of the verify operation but that verify operation is paused.


//REBUILD command definition
#define REBUILD_START               0x80                    //Rebuild start operation
#define REBUILD_STOP                0x81                    //Rebuild stop operation
#define REBUILD_PAUSE               0x82                    //Rebuild pause operation
#define REBUILD_RESUME              0x83                    //Rebuild resume operation
#define REBUILD_VOLUME0             0x01                    //Bit 0 = 1 - Volume 0
#define REBUILD_VOLUME1             0x02                    //Bit 1 = 1 - Volume 1
#define REBUILD_VOLUME2             0x04                    //Bit 2 = 1 - Volume 2
#define REBUILD_VOLUME3             0x08                    //Bit 3 = 1 - Volume 3
#define REBUILD_MAXVOLUMES          0x0F                    //Max. 4 volumes

//VERIFY command definition
#define VERIFY_START                0x80                    //Verify start operation
#define VERIFY_STOP                 0x81                    //Verify stop operation
#define VERIFY_PAUSE                0x82                    //Verify pause operation
#define VERIFY_RESUME               0x83                    //Verify resume operation
#define VERIFY_VOLUME0              0x01                    //Bit 0 = 1 - Volume 0
#define VERIFY_VOLUME1              0x02                    //Bit 1 = 1 - Volume 1
#define VERIFY_VOLUME2              0x04                    //Bit 2 = 1 - Volume 2
#define VERIFY_VOLUME3              0x08                    //Bit 3 = 1 - Volume 3
#define VERIFY_MAXVOLUMES           0x0F                    //Max. 4 volumes

//GET ENCLOSURE STATUS command definition
typedef struct _ENCLOSURE_STATUS {
    INT16   Temperature;                                    //Enclosure temperature (Celsius degree), big endian
    UINT8   Rev0[2];                                        //Reserved 2 bytes
    UINT32  FanSpeed;                                       //Enclosure fan speed (RPM), big endian
} ENCLOSURESTATUS, *PENCLOSURESTATUS;

#define ENCLOSURE_STATUS_LENGTH     sizeof(ENCLOSURESTATUS)

//GET TOPOLOGY command definition
//Topology map structure - 8 bytes
typedef struct _TOPOLOGY_MAP {
    UINT8   Layer;                                          //Layer: 
                                                            //  My cascade layer in MyTopology
                                                            //  Next cascade layer in PxNextTopology
    UINT8   P0Status;                                       //Port 0 connected status:
                                                            //  My Port 0 connected status in MyTopology
                                                            //  Next layer Port 0 connected status in PxNextTopology
    UINT8   P1Status;                                       //Port 1 connected status
    UINT8   P2Status;                                       //Port 2 connected status
    UINT8   P3Status;                                       //Port 3 connected status
    UINT8   P4Status;                                       //Port 4 connected status
    UINT8   Rev[2];                                         //Reserved
} TOPOLOGYMAP, *PTOPOLOGYMAP;

#define NC                          0x00                    //Not Connect an ASM109x (NC)
#define CA                          0x01                    //Connected an ASM109x (CA)
#define LAYER0                      0x00                    //Cascade Layer 0
#define LAYER1                      0x01                    //Cascade Layer 1
#define LAYER2                      0x02                    //Cascade Layer 2
#define LAYER3                      0x03                    //Cascade Layer 3
#define LAYER4                      0x04                    //Cascade Layer 4
#define TOPOLOGY_MAP_LENGTH         sizeof(TOPOLOGYMAP)

//Topology structure - 64 bytes
typedef struct _TOPOLOGY_ {
    UINT8           MapStatus;                              //Cascade Topology Map Status
    UINT8           MyLayer;                                //My cascade layer
    UINT8           Rev0[6];                                //Reserved 6 bytes;
    TOPOLOGYMAP     MyTopology;                             //My topology map
    TOPOLOGYMAP     P0NextTopology;                         //Port 0 topology map
    TOPOLOGYMAP     P1NextTopology;                         //Port 1 topology map
    TOPOLOGYMAP     P2NextTopology;                         //Port 2 topology map
    TOPOLOGYMAP     P3NextTopology;                         //Port 3 topology map
    TOPOLOGYMAP     P4NextTopology;                         //Port 4 topology map
    UINT8           Rev1[8];                                //Reserved 8 bytes;
} TOPOLOGY, *PTOPOLOGY;

#define MAP_NOT_AVAILABLE           0x00                    //Topology map is not available
#define MAP_AVAILABLE               0x01                    //Topology map is available
#define TOPOLOGY_LENGTH             sizeof(TOPOLOGY)


// SPI Offset or Abs
#define SPI_OFFSET			0x00                       //SPI Offset
#define SPI_ABS				0x01                       //SPI Abs


typedef struct DEVICE_TABLE_ {
	CString		path;
	CString		name;
	UINT64		address;
	bool		identify109;
	bool		exist_physical;
	bool		exist_table;
	UINT16		deviceSize_GB;
	UINT64		ChipSerialNum;
	CString		SN;
	CAPABILITIES	capaStatus;
	RAIDSTATUS		raidStauts ;
	DISKSTATUS		diskStaus;
	int			lastNotification;

} DEVICE_TABLE, *PDEVICE_TABLE;


typedef struct THREAD_TABLE_AUTO_ {
	int		time;				//min
	float	forecaseProgress;	//persentage
	float	realProgress;		//persentage
	int		detect;
} THREAD_TABLE_AUTO, *PTHREAD_TABLE_AUTO;


typedef struct mail_Sender{
	BOOL	exist;
	char	strToText[40];
	char	strFromText[40];
	char	strSubject[40];

	char	strSmtpText[40];
	char	strSmtpPort[6];
	char	strSmtpUserName[40];
	char	strPasswordText[40];
}mail_Sender;

typedef struct emailNotification
{	
	int eventIndex;
	int eventNumber;
	CString eventLog;	
}emailNotificationStructure, *pEmailNotificationStructure;

#define serailNUM_NON 0xff

#define DEVICE_NUM	16


#define NOTIFICATION_RAID_CREATED			301
#define NOTIFICATION_RAID_DELETE			302
#define NOTIFICATION_RAID_DEGRADED			303
#define NOTIFICATION_RAID1_REBUILD_FINISH	304
#define NOTIFICATION_RAID1_REBUILD_FAIL		305
#define NOTIFICATION_RAID_BROKEN			306
#define	NOTIFICATION_DATA_LOST				307
#define NOTIFICATION_DISK_PLUG_IN			308
#define NOTIFICATION_DISK_PLUG_OUT			309

#define NOTIFICATION_ERROR_VIRTUAL_HDD		311
#define NOTIFICATION_STATUS_GOOD			312



#define NOT_CHANGE_STATUS				800
#define NON_DEFININITION				801