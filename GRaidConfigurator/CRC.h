#ifndef _CRC_H
#define _CRC_H

#include	"ASM2109.h"

UINT32 GetCrc32(UINT8* pBuffer, UINTN BufferLength);

#endif  //_CRC_H