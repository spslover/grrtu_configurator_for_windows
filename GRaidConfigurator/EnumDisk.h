#pragma once



//
// Bus Type
//

static char* BusType[] = {
    "UNKNOWN",  // 0x00
    "SCSI",
    "ATAPI",
    "ATA",
    "IEEE 1394",
    "SSA",
    "FIBRE",
    "USB",
    "RAID"
};

//
// SCSI Device Type
//

static char* DeviceType[] = {
    "Direct Access Device", // 0x00
    "Tape Device",          // 0x01
    "Printer Device",       // 0x02
    "Processor Device",     // 0x03
    "WORM Device",          // 0x04
    "CDROM Device",         // 0x05
    "Scanner Device",       // 0x06
    "Optical Disk",         // 0x07
    "Media Changer",        // 0x08
    "Comm. Device",         // 0x09
    "ASCIT8",               // 0x0A
    "ASCIT8",               // 0x0B
    "Array Device",         // 0x0C
    "Enclosure Device",     // 0x0D
    "RBC Device",           // 0x0E
    "Unknown Device"        // 0x0F
};

extern void PrintError( ULONG );
extern void PrintDataBuffer( PUCHAR, ULONG );
extern void PrintStatusResults( BOOL, DWORD, PSCSI_PASS_THROUGH_WITH_BUFFERS );
extern void PrintSenseInfo( PSCSI_PASS_THROUGH_WITH_BUFFERS );
extern BOOL GetRegistryProperty( HDEVINFO, DWORD );
extern BOOL GetDeviceProperty( HDEVINFO, DWORD );
extern void DebugPrint( USHORT, PCHAR, ... );

extern void RegisterDeviceNotification(HWND hWnd);
extern void UnRegisterDeviceNotification(void);
extern void EnumDevice();

