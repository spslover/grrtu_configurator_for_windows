#include "stdafx.h"
#include "MultiLanguage.h"


MultiLanguageStr::MultiLanguageStr(){
	setLanguageEng();
}

MultiLanguageStr::~MultiLanguageStr(){

}

void MultiLanguageStr::setLanguageChi(){
	strMainDlgMessageMessage.Format("%s", CHI_MAIN_DLG_MESSAGE_MESSAGE);
	strMainDlgFirmwareVersion.Format("%s", CHI_MAIN_DLG_FIRMWARE_VERSION);
	strMainDlgFirmareVersionNoDevice.Format("%s",CHI_MAIN_DLG_FIRMWARE_VERSION_NO_DEVICE);
	strMainDlgContorlListControl.Format("%s", CHI_MAIN_DLG_CONTORL_LIST_CONTROL);
	strMainDlgMessageVHDDReason.Format("%s", CHI_MAIN_DLG_MESSAGE_VHDD_REASON);

	strMainDlgMessageRaidMode.Format("%s", CHI_MAIN_DLG_MESSAGE_RAID_MODE);
	strMainDlgMessageRaidStatus.Format("%s", CHI_MAIN_DLG_MESSAGE_RAID_STATUS);;
	strMainDlgMessageTemperature.Format("%s", CHI_MAIN_DLG_MESSAGE_TEMPERATURE);;
	strMainDlgMessageDeviceSN.Format("%s", CHI_MAIN_DLG_MESSAGE_DEVICE_SN);
	strMainDlgMessageFanSpeed.Format("%s", CHI_MAIN_DLG_MESSAGE_FAN_SPEED);
	strMainDlgMessageDeviceCapacity.Format("%s", CHI_MAIN_DLG_MESSAGE_DEVICE_CAPACITY);
	strMainDlgMessagePort0.Format("%s", CHI_MAIN_DLG_MESSAGE_PORT_0);
	strMainDlgMessagePort1.Format("%s", CHI_MAIN_DLG_MESSAGE_PORT_1);
	strMainDlgMessagePort2.Format("%s", CHI_MAIN_DLG_MESSAGE_PORT_2);
/*	strMainDlgMessageDeviceName0.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_NAME_0);
	strMainDlgMessageDeviceName1.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_NAME_1);
	strMainDlgMessageDeviceName2.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_NAME_2);	
*/
	strModeChangeDlgCurrentMode.Format("%s", CHI_MODE_CHANGE_DLG_CURRENT_MODE);
	strModeChangeDlgDiskAmount.Format("%s", CHI_MODE_CHANGE_DLG_DISK_AMOUNT);
	strModeChangeDlgChangeMode.Format("%s", CHI_MODE_CHANGE_DLG_CHANGE_MODE);

	strModeChangeDlgChangeFinish.Format("%s", CHI_MODE_CHANGE_DLG_MODE_CHANGE_FINISH);
	strModeChangeDlgChangeFail.Format("%s", CHI_MODE_CHANGE_DLG_MODE_CHANGE_FAIL);
	strModeChangeDlgChangeSelect.Format("%s",CHI_MODE_CHANGE_DLG_MODE_CHANGE_SELECT);

	strPsudoDeviceCapabilities00.Format("%s", CHI_PSUDO_DEVICE_STR_CAPABILITIES_00);
	strPsudoDeviceCapabilitiesFF.Format("%s", CHI_PSUDO_DEVICE_STR_CAPABILITIES_FF);

	strPsudoDevicRaidStatusRaidStatus00.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_00);
	strPsudoDevicRaidStatusRaidStatus01.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_01);
	strPsudoDevicRaidStatusRaidStatus02.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_02);
	strPsudoDevicRaidStatusRaidStatus03.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_03);
	strPsudoDevicRaidStatusRaidStatus04.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_04);
	strPsudoDevicRaidStatusRaidStatus05.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_05);
	strPsudoDevicRaidStatusRaidStatus06.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_06);
	strPsudoDevicRaidStatusRaidStatus07.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_07);
	strPsudoDevicRaidStatusRaidStatus08.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_08);
	strPsudoDevicRaidStatusRaidStatus09.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_09);
	strPsudoDevicRaidStatusRaidStatus10.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_10);
	strPsudoDevicRaidStatusRaidStatus11.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_11);

	strPsudoDevicRaidStatusPortStatus00.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_00);
	strPsudoDevicRaidStatusPortStatus01.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_01);
	strPsudoDevicRaidStatusPortStatus02.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_02);
	strPsudoDevicRaidStatusPortStatus03.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_03);
	strPsudoDevicRaidStatusPortStatus04.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_04);
	strPsudoDevicRaidStatusPortStatus05.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_05);
	strPsudoDevicRaidStatusPortStatus06.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_06);
	strPsudoDevicRaidStatusPortStatus07.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_07);
	strPsudoDevicRaidStatusPortStatus08.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_08);
	strPsudoDevicRaidStatusPortStatus09.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_09);
	strPsudoDevicRaidStatusPortStatus0A.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0A);
	strPsudoDevicRaidStatusPortStatus0B.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0B);
	strPsudoDevicRaidStatusPortStatus0C.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0C);
	strPsudoDevicRaidStatusPortStatus11.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_11);
	strPsudoDevicRaidStatusPortStatus1B.Format("%s", CHI_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_1B);

	strCurrentVhddReason00.Format("%s", CHI_CURRENT_VHDD_REASON_00);
	strCurrentVhddReason10.Format("%s", CHI_CURRENT_VHDD_REASON_10);
	strCurrentVhddReason11.Format("%s", CHI_CURRENT_VHDD_REASON_11);
	strCurrentVhddReason12.Format("%s", CHI_CURRENT_VHDD_REASON_12);
	strCurrentVhddReason13.Format("%s", CHI_CURRENT_VHDD_REASON_13);
	strCurrentVhddReason14.Format("%s", CHI_CURRENT_VHDD_REASON_14);
	strCurrentVhddReason15.Format("%s", CHI_CURRENT_VHDD_REASON_15);
	strCurrentVhddReason20.Format("%s", CHI_CURRENT_VHDD_REASON_20);
	strCurrentVhddReason30.Format("%s", CHI_CURRENT_VHDD_REASON_30);

}

void MultiLanguageStr::setLanguageEng(){
	strMainDlgMessageMessage.Format("%s", ENG_MAIN_DLG_MESSAGE_MESSAGE);
	strMainDlgFirmwareVersion.Format("%s", ENG_MAIN_DLG_FIRMWARE_VERSION);
	strMainDlgFirmareVersionNoDevice.Format("%s",ENG_MAIN_DLG_FIRMWARE_VERSION_NO_DEVICE);
	strMainDlgContorlListControl.Format("%s", ENG_MAIN_DLG_CONTORL_LIST_CONTROL);
	strMainDlgMessageVHDDReason.Format("%s", ENG_MAIN_DLG_MESSAGE_VHDD_REASON);

	strMainDlgMessageRaidMode.Format("%s", ENG_MAIN_DLG_MESSAGE_RAID_MODE);
	strMainDlgMessageRaidStatus.Format("%s", ENG_MAIN_DLG_MESSAGE_RAID_STATUS);;
	strMainDlgMessageTemperature.Format("%s", ENG_MAIN_DLG_MESSAGE_TEMPERATURE);;
	strMainDlgMessageDeviceSN.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_SN);
	strMainDlgMessageFanSpeed.Format("%s", ENG_MAIN_DLG_MESSAGE_FAN_SPEED);
	strMainDlgMessageDeviceCapacity.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_CAPACITY);
	strMainDlgMessagePort0.Format("%s", ENG_MAIN_DLG_MESSAGE_PORT_0);
	strMainDlgMessagePort1.Format("%s", ENG_MAIN_DLG_MESSAGE_PORT_1);
	strMainDlgMessagePort2.Format("%s", ENG_MAIN_DLG_MESSAGE_PORT_2);
/*	strMainDlgMessageDeviceName0.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_NAME_0);
	strMainDlgMessageDeviceName1.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_NAME_1);
	strMainDlgMessageDeviceName2.Format("%s", ENG_MAIN_DLG_MESSAGE_DEVICE_NAME_2);
*/	
	strModeChangeDlgCurrentMode.Format("%s", ENG_MODE_CHANGE_DLG_CURRENT_MODE);
	strModeChangeDlgDiskAmount.Format("%s", ENG_MODE_CHANGE_DLG_DISK_AMOUNT);
	strModeChangeDlgChangeMode.Format("%s", ENG_MODE_CHANGE_DLGE_CHANGE_MODE);
	
	strModeChangeDlgChangeFinish.Format("%s", ENG_MODE_CHANGE_DLG_MODE_CHANGE_FINISH);
	strModeChangeDlgChangeFail.Format("%s", ENG_MODE_CHANGE_DLG_MODE_CHANGE_FAIL);
	strModeChangeDlgChangeSelect.Format("%s",ENG_MODE_CHANGE_DLG_MODE_CHANGE_SELECT);

	strPsudoDeviceCapabilities00.Format("%s", ENG_PSUDO_DEVICE_STR_CAPABILITIES_00);
	strPsudoDeviceCapabilitiesFF.Format("%s", ENG_PSUDO_DEVICE_STR_CAPABILITIES_FF);

	strPsudoDevicRaidStatusRaidStatus00.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_00);
	strPsudoDevicRaidStatusRaidStatus01.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_01);
	strPsudoDevicRaidStatusRaidStatus02.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_02);
	strPsudoDevicRaidStatusRaidStatus03.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_03);
	strPsudoDevicRaidStatusRaidStatus04.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_04);
	strPsudoDevicRaidStatusRaidStatus05.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_05);
	strPsudoDevicRaidStatusRaidStatus06.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_06);
	strPsudoDevicRaidStatusRaidStatus07.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_07);
	strPsudoDevicRaidStatusRaidStatus08.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_08);
	strPsudoDevicRaidStatusRaidStatus09.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_09);
	strPsudoDevicRaidStatusRaidStatus10.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_10);
	strPsudoDevicRaidStatusRaidStatus11.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_RAIDSTATUS_11);

	strPsudoDevicRaidStatusPortStatus00.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_00);
	strPsudoDevicRaidStatusPortStatus01.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_01);
	strPsudoDevicRaidStatusPortStatus02.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_02);
	strPsudoDevicRaidStatusPortStatus03.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_03);
	strPsudoDevicRaidStatusPortStatus04.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_04);
	strPsudoDevicRaidStatusPortStatus05.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_05);
	strPsudoDevicRaidStatusPortStatus06.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_06);
	strPsudoDevicRaidStatusPortStatus07.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_07);
	strPsudoDevicRaidStatusPortStatus08.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_08);
	strPsudoDevicRaidStatusPortStatus09.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_09);
	strPsudoDevicRaidStatusPortStatus0A.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0A);
	strPsudoDevicRaidStatusPortStatus0B.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0B);
	strPsudoDevicRaidStatusPortStatus0C.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_0C);
	strPsudoDevicRaidStatusPortStatus11.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_11);
	strPsudoDevicRaidStatusPortStatus1B.Format("%s", ENG_PSUDO_DEVICE_STR_RAIDSTATUS_PORTSTATUS_1B);	

	strCurrentVhddReason00.Format("%s", ENG_CURRENT_VHDD_REASON_00);
	strCurrentVhddReason10.Format("%s", ENG_CURRENT_VHDD_REASON_10);
	strCurrentVhddReason11.Format("%s", ENG_CURRENT_VHDD_REASON_11);
	strCurrentVhddReason12.Format("%s", ENG_CURRENT_VHDD_REASON_12);
	strCurrentVhddReason13.Format("%s", ENG_CURRENT_VHDD_REASON_13);
	strCurrentVhddReason14.Format("%s", ENG_CURRENT_VHDD_REASON_14);
	strCurrentVhddReason15.Format("%s", ENG_CURRENT_VHDD_REASON_15);
	strCurrentVhddReason20.Format("%s", ENG_CURRENT_VHDD_REASON_20);
	strCurrentVhddReason30.Format("%s", ENG_CURRENT_VHDD_REASON_30);
}