// dbg.h: The debug header file.
#ifndef __DBG_H__
#define __DBG_H__

#include <windows.h>
#include <stdio.h>
#include <stdarg.h>


/******************************************************************************************************************************/
// Test Feature
#ifdef _DEBUG
#define	FEATURE_LOGIN_DIALOG_CHECK	ENABLE
#else
#define	FEATURE_LOGIN_DIALOG_CHECK	ENABLE
#endif
#define FEATURE_UTILITY_ENABLE_MP					ENABLE					//SIMULATION mode
#define FEATURE_UTILITY_ENABLE_103MPT				ENABLE					//Check AsmtUSBDev.h  "6U" for 2103

#define FEATURE_UTILITY_ENABLE_DetaiHostControl 	ENABLE					//Detai Host Control
#define FEATURE_UTILITY_ENABLE_ScanFromHCD		 	ENABLE					//Detai Host Control

#define FEATURE_UTILITY_ENABLE_AutoUpdate			DISABLE					//For AutoMode
#define FEATURE_UTILITY_ENABLE_INIT_SCAN			DISABLE					//1.25 AutoUpdate ENABLE -> Set INIT_SCAN ENABLE

#define FEATURE_UTILITY_ENABLE_WEEKBLOCK			ENABLE 					//1.11 WEEKBLOCK
#define FEATURE_UTILITY_ENABLE_FastPage				DISABLE					//1.19
#define FEATURE_PHYSICALRDWRTEST					ENABLE					//1.20
#define FEATURE_UTILITY_ENABLE_SACN_WIN8_UASP		ENABLE					//1.24
#define FEATURE_UTILITY_ENABLE_NEW_SLC_FREE_TABLE	DISABLE					//1.25
#define FEATURE_UTILITY_ENABLE_RETRY				ENABLE					//1.25

/******************************************************************************************************************************/
#ifdef _DEBUG
static void __dbg_printf (const char * format,...)
{

#define MAX_DBG_MSG_LEN (1024)
	char buf[MAX_DBG_MSG_LEN];
	va_list ap;
	va_start(ap, format);
	_vsnprintf_s( buf, sizeof(buf) , sizeof(buf) , format, ap);
	OutputDebugString(buf);
	va_end(ap);
}
#define DBG __dbg_printf
#else

#if (0)
	static void __dbg_printf (const char * format,...)
	{
	#define MAX_DBG_MSG_LEN (1024)
		char buf[MAX_DBG_MSG_LEN];
		va_list ap;
		va_start(ap, format);
		_vsnprintf_s( buf, sizeof(buf) , sizeof(buf) , format, ap);
		::AfxMessageBox(buf , 0 , 0 );
		va_end(ap);
	}
#else
	static void __dbg_printf (const char * format,...)
	{
	}
	#define DBG  1?((void)(NULL)):__dbg_printf
#endif

//#define DBG __dbg_printf

#endif

#define _FLP_ "\r\n%s:%d: "
#define _FL_  __FUNCTION__ , __LINE__

#define _FFLP_ "%s:" _FLP_
#define _FFL_ __FILE__, _FL_

// For example:
// DBG(_FLP_ "format string", _FL_, arg1, arg2);
// DBG(_FFLP_ "format string", _FFL, arg1, arg2);
#endif
