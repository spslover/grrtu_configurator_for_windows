#pragma once

#include "stdafx.h"
// sps
//#include "109GUIv2.h"
#include "ASM2109.h"


#define IDENTIFY_TIMEOUT        100      //Identyfy command timeout 0.1 ms
#define COMMAND_TIMEOUT         2000      //other command timeout 2	ms
#define DISCONNECT_TIMEOUT		1000	  //DISCONNECT timeout 1 ms

#define		DRIVE_COUNT			16U         // Total Drive Count
#define     MAX_DEV_NUM			24U         // Max Dev Num

#define MAIN_RESULT_GUI_PORTNUMBER      0
#define MAIN_RESULT_GUI_STATUS          1
#define MAIN_RESULT_GUI_MODELNUMBER     2
#define MAIN_RESULT_GUI_SERIALNUMBER    3
#define MAIN_RESULT_GUI_4				4
#define MAIN_RESULT_GUI_5				5
#define MAIN_RESULT_GUI_6				6
#define MAIN_RESULT_GUI_7				7
#define MAIN_RESULT_GUI_8				8
#define MAIN_RESULT_GUI_9				9  
#define MAIN_RESULT_GUI_10				10
#define MAIN_RESULT_GUI_11				11
#define MAIN_RESULT_GUI_12				12

#define COMMAND_SEQENTIAL				0x10
#define COMMAND_BUFFER					0x20

/************************************************************************************************
*																								*
*	2012/10/25	 TestTool 1.2   Jimmy Hu	Add Buffer Read Method
*	2012/10/25	 TestTool 1.1   Jimmy Hu	Add CheckCascadeParameters function.				*
*											Add GET TOPOLOGY command.							*
*											Modify CascadeLayer and CascadePort for every		*
*												commands to sync. V0.91 document.  				*																		
*																								*
*																								*
************************************************************************************************/


class cmdPacket{

public:
	cmdPacket();
	~cmdPacket();
	/*********************************************Sequential*********************************************/
	IFSTATUS IFCmdIdentify	(void);

	IFSTATUS IFCmdReadFlashRom(UINT8 CascadeLayer, UINT8 CascadePort, UINT32 RomAddress, UINT8* pReadData, BOOL AbsoluteAddr);
	IFSTATUS IFCmdEraseFlashRomSector(UINT8 CascadeLayer, UINT8 CascadePort, UINT32 RomAddress, BOOL AbsoluteAddr);
	IFSTATUS IFCmdWriteFlashRom(UINT8 CascadeLayer, UINT8 CascadePort, UINT32 RomAddress, UINT8* pWriteData, BOOL AbsoluteAddr);
	IFSTATUS IFCmdReset(UINT8 CascadeLayer, UINT8 CascadePort, UINT16 DisconnectTime);
	IFSTATUS IFCmdGetRegister(UINT8 CascadeLayer, UINT8 CascadePort, UINT16 RegAddress, UINT8* pRegValue);
	IFSTATUS IFCmdSetRegister(UINT8 CascadeLayer, UINT8 CascadePort, UINT16 RegAddress, UINT8 RegValue);

	IFSTATUS IFCmdBistStart(UINT8 CascadeLayer, UINT8 CascadePort, UINT8 DevicePorts, UINT8 SATAGenerations, UINT8 TestTime);
	IFSTATUS IFCmdBistStop(UINT8 CascadeLayer, UINT8 CascadePort, PBISTRESULTS pBistResults);
	IFSTATUS IFCmdGetCapabilities(UINT8 CascadeLayer, UINT8 CascadePort, PCAPABILITIES pCap);

	IFSTATUS IFCmdModeChange(UINT8 CascadeLayer, UINT8 CascadePort, PRAIDPARAMETER pRaidParameter, UINT16 disconnetTime);
	IFSTATUS IFCmdGetRAIDStatus(UINT8 CascadeLayer, UINT8 CascadePort, PRAIDSTATUS pRaidStatus);
	IFSTATUS IFCmdGetDiskStatus(UINT8 CascadeLayer, UINT8 CascadePort, PDISKSTATUS pDiskStatus);
	IFSTATUS IFCmdRebuild(UINT8 CascadeLayer, UINT8 CascadePort, UINT8 Operation, UINT8 Volume);	// no buffer
	IFSTATUS IFCmdVerify(UINT8 CascadeLayer, UINT8 CascadePort, UINT8 Operation, UINT8 Volume);		// no buffer
	IFSTATUS IFCmdGetEnclosureStatus(UINT8 CascadeLayer, UINT8 CascadePort, PENCLOSURESTATUS pEnclosureStatus);
	IFSTATUS IFCmdGetTopology(PTOPOLOGY pTopology);

	/*********************************************Buffer start****************************************/
	
	BOOL UpdateBoth(INT32 FileLength, UINT8* pBuf);	
	BOOL RWBuffer(bool bRW,BYTE bDiskNumber,unsigned char* buf);
	void CmdPack2Buffer(PIFACKDATA pCmdPack,UINT8* pByte,bool bData);
	void vUpdateMainResultItemTable(int iColumn,int iItemIdx,CString strValue);


	/*********************************************Buffer end******************************************/



	CString getString		(void);
	CString getStatusEx		(void);
	CString getAckLBAEx		(void);
	CString getAckStatusEx	(void);
	CString getLBAElementEx	(void);
	CString getLBAEx		(void);
	void	clearBuffer		(void);
	void	clearPathHandle	(void);
	void    setHandle		(int deviceOrder);
	void    setHandle		(CString path);

	int		sectorSize;
	int		deviceNum;

	int		commandType;

	HANDLE	hDevice; 
	CString	devicePath;

	IFSTATUS excutionStatus;

public:	//switch parameter
	IFACKDATA	ACKtemp;


private:
	void		BTLExchange		(UINT8* pData, UINTN Length);
	IFSTATUS	SendPacket		(void);
	IFSTATUS	SendPhysicalLBA	(void);
	IFSTATUS	ReadAck			(PCOMMANDPACKET pCmdPack);
	IFSTATUS	GetAckStatus	(UINT8 status);
	IFSTATUS	GetAckData		(UINTN WaitTime);
	void		FillOutPacketHeader(PIFHEADER pHeader);

	BOOL		CheckCascadeParameters(UINT8 CascadeLayer, UINT8 CascadePort);

	bool		sendLBA(DWORD shiftLBA , HANDLE hDevice, BYTE  lpBuf[]);
	
	CString	cStr;
	UINT8	DataField;
	
	PCOMMANDPACKET pCmdPack;


	/*********************************************Buffer start****************************************/
	CString	strMPToolDlgOnInitDialogMainResultListCtrlPASS;
	CString	strMPToolDlgOnInitDialogMainResultListCtrlFAIL;
	CListCtrl m_ctrlListMainResult;
	/*********************************************Buffer end******************************************/
};

