#include "stdafx.h"
#include "CRC.h"


/**************************************************************************************
*Filename: crc.c
*Description:
*   Calculate 32 bits CRC. Modify from 2105 MPTool function
*
*History:
*   2012/07/24  V1.0    Jesse Chang     First version
*
*                                                   Asmedia Technology, Inc
***************************************************************************************/

UINT32  gUiCRCTab[256];             //CRC initial table

//
//Procedure:    Reflect
//Description:  Reflect 32 bits value. Only called by Crc32Init 
//Input:    ReflectValue    - Value to reflect
//          ReflectLength   - Length to reflect
//Output:   Reflected value
//
UINT32 Reflect(UINT32 ReflectValue, UINT8 ReflectLength)
{
//Used only by Init_CRC32_Table()
    UINT32  value = 0;
    UINTN   i;

    // Swap bit 0 for bit 7
    // bit 1 for bit 6, etc.
    for(i = 1; i < (UINTN)(ReflectLength + 1); i++)
    {
        if(ReflectValue & 1)
        {
            value |= 1L << (ReflectLength - i);
        }
        ReflectValue >>= 1;
    }
    return value;
}


//
//Procedure:    Crc32Init
//Description:  Init CRC32 table 
//Input:    None
//Output:   None
//
void Crc32Init()
{
    UINT32  ulPolynomial = 0x04c11db7L;
    UINTN   i, j;

    // 256 values representing ASCII character codes.
    for(i = 0; i <= 0xFF; i++)
    {
        gUiCRCTab[i] = Reflect(i, 8) << 24;
        for (j = 0; j < 8; j++)
        {
            gUiCRCTab[i] = (gUiCRCTab[i] << 1) ^ (gUiCRCTab[i] & (1L << 31) ? ulPolynomial : 0);
            //gUiCRCTab[i] = (gUiCRCTab[i] << 1) ^ (gUiCRCTab[i] & (0x80000000L) ? ulPolynomial : 0);
        }
        gUiCRCTab[i] = Reflect(gUiCRCTab[i], 32);
    }
}

 
//
//Procedure:    GetCrc32
//Description:  Get 32 bits CRC. 
//Input:    pBuffer         - Data buffer to calculate CRC32
//          BufferLength    - Data buffer length 
//Output:   32 bits CRC
//
UINT32 GetCrc32(UINT8* pBuffer, UINTN BufferLength)
{
    UINT32  crc = 0xFFFFFFFFL;
    UINTN   len;

    Crc32Init();                //Initial CRC32 table
    len = BufferLength;
    // Perform the algorithm on each character
    // in the string, using the lookup table values.
    while(len--)
    {
        crc = (crc >> 8) ^ gUiCRCTab[(crc & 0xFFL) ^ *pBuffer++];
    }
    // Exclusive OR the result with the beginning value.
    return (crc^0xFFFFFFFFL);
}
