#include "stdafx.h"

#include "APT.h"
#include "dbg.h"

#define ATA_FLAGS_DRDY_REQUIRED         (1 << 0)
#define ATA_FLAGS_DATA_IN               (1 << 1)
#define ATA_FLAGS_DATA_OUT              (1 << 2)
#define ATA_FLAGS_48BIT_COMMAND         (1 << 3)
#define ATA_FLAGS_USE_DMA               (1 << 4)
#define ATA_FLAGS_NO_MULTIPLE           (1 << 5)

#define READ_BUFFER					0xE4                       //Read Buffer Command
#define WRITE_BUFFER				0xE8                       //Write Buffer Command
#define	ATA_PASS_THROUGH_BY_SCSI_CMD_TIME_OUT_VALUE		65535

BOOL SendScsiCmdATAPassTroughCDB12(BYTE CDB12[] , 
									 HANDLE	hDevice , BYTE  lpBuf[] , BYTE DataInOut, DWORD *pdwBufferLen){
	SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;

	DWORD	dwRet = 0;
	DWORD	dwInBufferSize 	= 0;
	DWORD	dwOutBufferSize	= 0;
	BOOL	bResult = FALSE;
	DWORD	dwErrorCode;
	//INT		iResult;

	ZeroMemory(&sptwb, sizeof(sptwb) );

	//Fill in the query data
	sptwb.Spt.Length                    = sizeof(SCSI_PASS_THROUGH);
	sptwb.Spt.CdbLength                 = 12U;
	sptwb.Spt.SenseInfoLength           = 24U;
	sptwb.Spt.DataTransferLength        = *pdwBufferLen;
	sptwb.Spt.TimeOutValue              = ATA_PASS_THROUGH_BY_SCSI_CMD_TIME_OUT_VALUE;
	sptwb.Spt.DataBufferOffset          = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
	sptwb.Spt.SenseInfoOffset           = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
	sptwb.Spt.DataIn					= DataInOut;


	sptwb.Spt.Cdb[0] = CDB12[0];
	sptwb.Spt.Cdb[1] = CDB12[1];
	sptwb.Spt.Cdb[2] = CDB12[2];
	sptwb.Spt.Cdb[3] = CDB12[3];
	sptwb.Spt.Cdb[4] = CDB12[4];

	sptwb.Spt.Cdb[5] = CDB12[5];
	sptwb.Spt.Cdb[6] = CDB12[6];
	sptwb.Spt.Cdb[7] = CDB12[7];
	sptwb.Spt.Cdb[8] = CDB12[8];
	sptwb.Spt.Cdb[9] = CDB12[9];

	sptwb.Spt.Cdb[10] = CDB12[10];
	sptwb.Spt.Cdb[11] = CDB12[11];

	memcpy( sptwb.ucDataBuf , lpBuf , *pdwBufferLen );

		dwInBufferSize  = sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS);
		dwOutBufferSize  = sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS);
/*
	switch(DataInOut)
	{
	case SCSI_IOCTL_DATA_IN:
		dwInBufferSize  = sptwb.Spt.DataTransferLength;
		dwOutBufferSize  = sptwb.Spt.DataTransferLength;
		break;
	case SCSI_IOCTL_DATA_OUT:
		dwInBufferSize  = sptwb.Spt.DataTransferLength;
		dwOutBufferSize  = sptwb.Spt.DataTransferLength;
		break;
	case SCSI_IOCTL_DATA_UNSPECIFIED:
		dwInBufferSize  = ( sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS) + sptwb.Spt.DataTransferLength );
		break;
	}
*/	
	dwOutBufferSize = ( offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf) + sptwb.Spt.DataTransferLength );


	if(hDevice != INVALID_HANDLE_VALUE)
	{
		if(DeviceIoControl(hDevice,IOCTL_SCSI_PASS_THROUGH,&sptwb,dwInBufferSize,&sptwb,dwOutBufferSize,&dwRet,FALSE) )
		{
			if( sptwb.Spt.ScsiStatus )
	        {
				bResult = FALSE;
				dwErrorCode = ::GetLastError();
	        }
			else
			{
				bResult = TRUE; 
				// Post-Doing
				if(DataInOut == SCSI_IOCTL_DATA_IN)
				{
					dwRet = dwRet - offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
					if(dwRet)
					{
					
						*pdwBufferLen = dwRet;
						memcpy( lpBuf , sptwb.ucDataBuf , dwRet );
					}
				}
/*
				switch(SubCommand)
				{
					case SCSI_VEN_FORCETOOLING_MODE:
					//case m_SendFWUpdateMode:
					case SCSI_VEN_DISK_USB_RESET_MODE:
						::memcpy( lpBuf , sptwb.ucDataBuf , *pdwBufferLen );
						break;

					//case m_SystemReadSectors:
					//case m_GetFlashStatusMode:
					//	::memcpy( lpBuf , sptwb.ucDataBuf , dwBufferLen );
					//	break;

					//case m_SystemWriteSectors:
					//case m_WriteClient_WriteSectors:
					//case m_WriteFlashConfig:
					//case m_SendDeviceInitialMode:
					//	break;

				default:
					bResult = FALSE;
					iResult = 0;//USBDEV_OTHER_CMD;
				}
*/
			}
		}
		
	}
	else
	{
	}
	return bResult;

}

BOOL SendScsiCmdATAPassTroughCDB16(BYTE CDB16[] , 
									 HANDLE	hDevice , BYTE  lpBuf[] , BYTE DataInOut, DWORD *pdwBufferLen){
	SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;

	DWORD	dwRet = 0;
	DWORD	dwInBufferSize 	= 0;
	DWORD	dwOutBufferSize	= 0;
	BOOL	bResult = FALSE;
	DWORD	dwErrorCode;
	//INT		iResult;

	ZeroMemory(&sptwb, sizeof(sptwb) );

	//Fill in the query data
	sptwb.Spt.Length                    = sizeof(SCSI_PASS_THROUGH);
	sptwb.Spt.CdbLength                 = 16U;
	sptwb.Spt.SenseInfoLength           = 24U;
	sptwb.Spt.DataTransferLength        = *pdwBufferLen;
	sptwb.Spt.TimeOutValue              = ATA_PASS_THROUGH_BY_SCSI_CMD_TIME_OUT_VALUE;
	sptwb.Spt.DataBufferOffset          = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
	sptwb.Spt.SenseInfoOffset           = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
	sptwb.Spt.DataIn					= DataInOut;


	sptwb.Spt.Cdb[0] = CDB16[0];
	sptwb.Spt.Cdb[1] = CDB16[1];
	sptwb.Spt.Cdb[2] = CDB16[2];
	sptwb.Spt.Cdb[3] = CDB16[3];
	sptwb.Spt.Cdb[4] = CDB16[4];

	sptwb.Spt.Cdb[5] = CDB16[5];
	sptwb.Spt.Cdb[6] = CDB16[6];
	sptwb.Spt.Cdb[7] = CDB16[7];
	sptwb.Spt.Cdb[8] = CDB16[8];
	sptwb.Spt.Cdb[9] = CDB16[9];

	sptwb.Spt.Cdb[10] = CDB16[10];
	sptwb.Spt.Cdb[11] = CDB16[11];
	sptwb.Spt.Cdb[11] = CDB16[12];
	sptwb.Spt.Cdb[11] = CDB16[13];
	sptwb.Spt.Cdb[11] = CDB16[14];
	sptwb.Spt.Cdb[11] = CDB16[15];


	memcpy( sptwb.ucDataBuf , lpBuf , *pdwBufferLen );

		dwInBufferSize  = sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS);
		dwOutBufferSize  = sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS);
/*
	switch(DataInOut)
	{
	case SCSI_IOCTL_DATA_IN:
		dwInBufferSize  = sptwb.Spt.DataTransferLength;
		dwOutBufferSize  = sptwb.Spt.DataTransferLength;
		break;
	case SCSI_IOCTL_DATA_OUT:
		dwInBufferSize  = sptwb.Spt.DataTransferLength;
		dwOutBufferSize  = sptwb.Spt.DataTransferLength;
		break;
	case SCSI_IOCTL_DATA_UNSPECIFIED:
		dwInBufferSize  = ( sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS) + sptwb.Spt.DataTransferLength );
		break;
	}
*/	
	dwOutBufferSize = ( offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf) + sptwb.Spt.DataTransferLength );


	if(hDevice != INVALID_HANDLE_VALUE)
	{
		if(DeviceIoControl(hDevice,IOCTL_SCSI_PASS_THROUGH,&sptwb,dwInBufferSize,&sptwb,dwOutBufferSize,&dwRet,FALSE) )
		{
			if( sptwb.Spt.ScsiStatus )
	        {
				bResult = FALSE;
				dwErrorCode = ::GetLastError();
	        }
			else
			{
				bResult = TRUE; 
				// Post-Doing
				if(DataInOut == SCSI_IOCTL_DATA_IN)
				{
					dwRet = dwRet - offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
					if(dwRet)
					{
					
						*pdwBufferLen = dwRet;
						memcpy( lpBuf , sptwb.ucDataBuf , dwRet );
					}
				}
/*
				switch(SubCommand)
				{
					case SCSI_VEN_FORCETOOLING_MODE:
					//case m_SendFWUpdateMode:
					case SCSI_VEN_DISK_USB_RESET_MODE:
						::memcpy( lpBuf , sptwb.ucDataBuf , *pdwBufferLen );
						break;

					//case m_SystemReadSectors:
					//case m_GetFlashStatusMode:
					//	::memcpy( lpBuf , sptwb.ucDataBuf , dwBufferLen );
					//	break;

					//case m_SystemWriteSectors:
					//case m_WriteClient_WriteSectors:
					//case m_WriteFlashConfig:
					//case m_SendDeviceInitialMode:
					//	break;

				default:
					bResult = FALSE;
					iResult = 0;//USBDEV_OTHER_CMD;
				}
*/
			}
		}
		
	}
	else
	{
	}
	return bResult;

}



BOOL SendATAPassThrough(HANDLE hDevice, BYTE bReadWrite, WORD wLength, BYTE Feature, BYTE SectorCnt, DWORD dwLBA, BYTE DevHead, BYTE Command, BYTE *pBuf)
{

	BOOL result; 
	DWORD dwRWSize;
	ATA_PASS_THROUGH_EX *papt; 
	BYTE *ATA_Buffer = new BYTE[sizeof(ATA_PASS_THROUGH_EX) + wLength]; 

	if(hDevice != INVALID_HANDLE_VALUE)
	{


		papt = (ATA_PASS_THROUGH_EX *)ATA_Buffer; 

		ZeroMemory(papt, sizeof(ATA_Buffer)); 
		papt->Length = sizeof(ATA_PASS_THROUGH_EX); 
		papt->AtaFlags = (bReadWrite == RW_READ)?ATA_FLAGS_DATA_IN:(bReadWrite == RW_WRITE)?ATA_FLAGS_DATA_OUT:0; 
		papt->DataTransferLength = wLength; 
		papt->TimeOutValue = 120; 
		papt->DataBufferOffset = sizeof(ATA_PASS_THROUGH_EX); 
		papt->PathId	= 0;
		papt->Lun		= 0;
		papt->TargetId	= 0;

		if(bReadWrite == RW_WRITE)
		{
			memcpy(ATA_Buffer + papt->DataBufferOffset, pBuf, wLength);
		}

		papt->CurrentTaskFile[0]= Feature; 
		papt->CurrentTaskFile[1]= SectorCnt;
		papt->CurrentTaskFile[2]= LOBYTE(LOWORD(dwLBA));
		papt->CurrentTaskFile[3]= HIBYTE(LOWORD(dwLBA));
		papt->CurrentTaskFile[4]= LOBYTE(HIWORD(dwLBA));
		papt->CurrentTaskFile[5]= DevHead | HIBYTE(HIWORD(dwLBA));
		papt->CurrentTaskFile[6]= Command;
		papt->CurrentTaskFile[7]= 0x00; 



		result = DeviceIoControl( hDevice, 
								  IOCTL_ATA_PASS_THROUGH, 
								  papt, sizeof(ATA_PASS_THROUGH_EX) + papt->DataTransferLength, 
								  papt, sizeof(ATA_PASS_THROUGH_EX) + papt->DataTransferLength, 
								  &dwRWSize, NULL );

		if(result)
		{
			if(bReadWrite == RW_READ)
			{
				memcpy(pBuf, &ATA_Buffer[papt->DataBufferOffset], wLength);
			}

			delete ATA_Buffer;

			return result;
		}
		
	}

	delete ATA_Buffer;
	return FALSE;

}


BOOL SendATAPassThroughDirect(HANDLE hDevice, BYTE bReadWrite, WORD wLength, BYTE Feature, BYTE SectorCnt, DWORD dwLBA, BYTE DevHead, BYTE Command, BYTE *pBuf)
{
	BOOL status = FALSE;
		
	if(hDevice != INVALID_HANDLE_VALUE)
	{

		ATA_PASS_THROUGH_DIRECT aptd; 
		ZeroMemory(&aptd,sizeof(ATA_PASS_THROUGH_DIRECT)); 
		aptd.Length	=	sizeof(ATA_PASS_THROUGH_DIRECT); 
		aptd.TargetId = 0; 
		aptd.Lun		= 0;
		aptd.PathId   = 0;
		aptd.DataTransferLength = wLength + sizeof(ATA_PASS_THROUGH_DIRECT); 
		aptd.TimeOutValue = 50; 
		aptd.DataBuffer = pBuf; 
	
		aptd.AtaFlags = (bReadWrite == RW_READ)?ATA_FLAGS_DATA_IN:(bReadWrite == RW_WRITE)?ATA_FLAGS_DATA_OUT:0;
		aptd.AtaFlags |= ATA_FLAGS_USE_DMA;
		aptd.CurrentTaskFile[0]= Feature; 
		aptd.CurrentTaskFile[1]= SectorCnt;
		aptd.CurrentTaskFile[2]= LOBYTE(LOWORD(dwLBA));
		aptd.CurrentTaskFile[4]= LOBYTE(HIWORD(dwLBA));
		aptd.CurrentTaskFile[5]= DevHead | HIBYTE(HIWORD(dwLBA));
		aptd.CurrentTaskFile[6]= Command;
		aptd.CurrentTaskFile[7]= 0x00; 
	
		ULONG returned = 0; 

		DWORD dwInLen = sizeof(ATA_PASS_THROUGH_DIRECT);
		DWORD dwOutLen = sizeof(ATA_PASS_THROUGH_DIRECT);

		if(bReadWrite == RW_READ)  dwInLen += wLength;
		if(bReadWrite == RW_WRITE) dwOutLen += wLength;

		status = DeviceIoControl( hDevice, IOCTL_ATA_PASS_THROUGH_DIRECT, 
								   &aptd, dwInLen, 
								   &aptd, dwOutLen, 
								   &returned, NULL); 
	}
	
	return status;

}

BOOL GetATA_Identify(HANDLE hDevice, IDINFO* pIdenInfo)
{
	BYTE status;

	BYTE ReadWrite	= RW_READ;
	WORD wLength	= 512;
	BYTE Feature	= 0;
	BYTE SectorCnt	= 0;
	DWORD dwLBA		= 0;
	BYTE DevHead	= 0xE0;
	BYTE Command	= 0xEC;
	
	status = SendATAPassThrough(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, (BYTE *)pIdenInfo);
	return status;	
}

BOOL GetAPI_Identify(HANDLE hDevice, IDINFO *deviceInfo)
{
	BOOL status;

	HANDLE	hIoCtrl;
	
	IDENTIFY_DEVICE_OUTDATA	sendCmdOutParam;
	SENDCMDINPARAMS	sendCmd;
	DWORD	dwReturned;

		hIoCtrl = hDevice;

		if(hIoCtrl == INVALID_HANDLE_VALUE)
		{
			return	FALSE;
		}
		::ZeroMemory(&sendCmdOutParam, sizeof(IDENTIFY_DEVICE_OUTDATA));
		::ZeroMemory(&sendCmd, sizeof(SENDCMDINPARAMS));

		sendCmd.irDriveRegs.bCommandReg			= ID_CMD;
		sendCmd.irDriveRegs.bSectorCountReg		= 1;
		sendCmd.irDriveRegs.bSectorNumberReg	= 1;
		sendCmd.irDriveRegs.bDriveHeadReg		= 0xA0;
		sendCmd.cBufferSize						= IDENTIFY_BUFFER_SIZE;

	//	DebugPrint(_T("SendAtaCommandPd - IDENTIFY_DEVICE"));
		status = ::DeviceIoControl(hIoCtrl, 0x0007C088, 
			&sendCmd, sizeof(SENDCMDINPARAMS),
			&sendCmdOutParam, sizeof(IDENTIFY_DEVICE_OUTDATA),
			&dwReturned, NULL);
		
		if(status == FALSE || dwReturned != sizeof(IDENTIFY_DEVICE_OUTDATA))
		{
			return	FALSE;
		}

//		DBG( _FLP_ "SendAtaCommandPd - IDENTIFY_DEVICE_Start ", _FL_ );

		memcpy_s(deviceInfo, sizeof(IDINFO), sendCmdOutParam.SendCmdOutParam.bBuffer, sizeof(IDINFO));
	
//		DBG( _FLP_ "SendAtaCommandPd - IDENTIFY_DEVICE_End ", _FL_ );
	return status;

}


BOOL GetSCSI_ATA_Identify(HANDLE hDevice, IDINFO *deviceInfo)
{
	BOOL status;
	BYTE bATAFlag			= 0;
	BYTE bCommandCurrent	= 0;
	CString str;

	BYTE CDB12[12]={0};
	DWORD size=8;

	UINT8 buf[512]={0};

	size = 512;
	CDB12[0] = 0xA1;//ATA PASS THROUGH(12) OPERATION CODE(A1h)
	CDB12[1] = (4 << 1) | 0; //MULTIPLE_COUNT=0,PROTOCOL=4(PIO Data-In),Reserved
	CDB12[2] = (1 << 3) | (1 << 2) | 2;//OFF_LINE=0,CK_COND=0,Reserved=0,T_DIR=1(ToDevice),BYTE_BLOCK=1,T_LENGTH=2
	CDB12[3] = 0;//FEATURES (7:0)
	CDB12[4] = 1;//SECTOR_COUNT (7:0)
	CDB12[5] = 0;//LBA_LOW (7:0)
	CDB12[6] = 0;//LBA_MID (7:0)
	CDB12[7] = 0;//LBA_HIGH (7:0)
	CDB12[8] = 0xa0;
	CDB12[9] = 0xEC;//COMMAND
		
	
	status = SendScsiCmdATAPassTroughCDB12(CDB12 ,hDevice , buf , SCSI_IOCTL_DATA_IN, &size);
	memcpy(deviceInfo, buf, 512);

	return status;
}
BOOL GetSCSI_Identify(HANDLE hDevice, IDINFO *deviceInfo){
	BOOL status;
	BYTE bATAFlag			= 0;
	BYTE bCommandCurrent	= 0;
	CString str;

	BYTE CDB12[12]={0};
	DWORD size=0;

	UINT64	deviceSize=0;

	bATAFlag =0;

	UINT8 buf[512]={0};

	UINT8 bRW = 0 ;// Read


	if(!bRW){					
		bCommandCurrent		= READ_BUFFER; //Read Buffer
		CDB12[0] = SCSIOP_INQUIRY;
		CDB12[1] = 0x00;
		CDB12[2] = 0x00;
		CDB12[9] = bCommandCurrent;
		size=60;
	}
	
	if(!bRW)					//Read,Write,NonData
		bATAFlag = bATAFlag|ATA_FLAGS_DATA_IN;	//Read

	bATAFlag = bATAFlag|ATA_FLAGS_DRDY_REQUIRED;//0
	
	if(!bRW){			//send ATA_pass_through_SCSI
		status = SendScsiCmdATAPassTroughCDB12(CDB12 ,hDevice , buf , SCSI_IOCTL_DATA_IN, &size);
	}
	
	CString deviceVendor= "";
	CString deviceProduct= "";
	
	for(int i=8; i<=15 || buf[i] == '\0 '; i++){
		deviceVendor += buf[i];
	}
 
	for(int i=16; i<=31 || buf[i] == '\0'; i++){
		deviceProduct += buf[i];
	}
	deviceVendor.Remove(' ');
	deviceProduct.Remove(' ');
	deviceVendor = deviceVendor + " " +deviceProduct;
	
	//DBG( _FLP_ "SendAtaCommandPd - GetSCSI_Identify_Start ", _FL_ );

	memcpy(deviceInfo->sModelNumber, deviceVendor.GetBuffer(), deviceVendor.GetLength() );

	//DBG( _FLP_ "SendAtaCommandPd - GetSCSI_Identify_Start ", _FL_ );

	memset(buf, 0x00, 512);
	if(!bRW){					
		bCommandCurrent		= READ_BUFFER; //Read Buffer
		CDB12[0] = SCSIOP_READ_CAPACITY;
		CDB12[1] = 0x00;
		CDB12[2] = 0x00;
		CDB12[9] = bCommandCurrent;
		size=8;
	}
	
	if(!bRW)					//Read,Write,NonData
		bATAFlag = bATAFlag|ATA_FLAGS_DATA_IN;	//Read

	bATAFlag = bATAFlag|ATA_FLAGS_DRDY_REQUIRED;//0
	
	if(!bRW){			//send ATA_pass_through_SCSI
		status = SendScsiCmdATAPassTroughCDB12(CDB12 ,hDevice , buf , SCSI_IOCTL_DATA_IN, &size);
	}
	
	for(int i=0; i<4 ; i++){
		deviceSize = deviceSize<<8;
		deviceSize |= buf[i];
	}

	if((deviceSize) != 0x00000000FFFFFFFF){
		deviceInfo->TotalNumberOfLogicalSectors = deviceSize;
		return status;
	}

	///////////////////////////////////////////////
	/*Device over 4T*/
	deviceSize = 0;
	BYTE CDB16[16]={0};


	memset(buf, 0x00, 512);	
	if(!bRW){					
		bCommandCurrent		= READ_BUFFER; //Read Buffer
		CDB16[0] = 0x9e;	//Read Capacity 16
		CDB16[1] = 0x00;
		CDB16[2] = 0x00;
		CDB16[15] = bCommandCurrent;
		size=12;
	}
//ATAFlag
	
	if(!bRW)					//Read,Write,NonData
		bATAFlag = bATAFlag|ATA_FLAGS_DATA_IN;	//Read
	bATAFlag = bATAFlag|ATA_FLAGS_DRDY_REQUIRED;//0
	
	if(!bRW){			//send ATA_pass_through_SCSI
		status = SendScsiCmdATAPassTroughCDB16(CDB16 ,hDevice , buf , SCSI_IOCTL_DATA_IN, &size);
	}

	for(int i=0; i<8 ; i++){
		deviceSize = deviceSize<<8;
		deviceSize |= buf[i];
	}
	deviceInfo->TotalNumberOfLogicalSectors = deviceSize;
	return status;
}


BOOL GetReadSector(HANDLE hDevice, DWORD dwLBA, BYTE SectorCnt,BYTE *pBuffer)
{
	BYTE status;

	BYTE ReadWrite	= RW_READ;
	WORD wLength	= SectorCnt * 512;
	BYTE Feature	= 0;
	//BYTE SectorCnt	= Sector;
	//DWORD dwLBA		= dwLBA;
	BYTE DevHead	= 0xE0;
	BYTE Command	= 0xC8;


	//status = SendATAPassThrough(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuffer);
	status = SendATAPassThroughDirect(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuffer);
	//status = SendATAPassThroughDirect(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, (BYTE *)pIdenInfo);	
	return status;	
}
BOOL SendATAPassThrough48BITS(HANDLE hDevice, BYTE bATAFlag, 
							  BYTE FeatureCurrent, BYTE SectorCurrent, DWORD dwLBACurrent, BYTE Device, BYTE Command, 
							  BYTE FeaturePrevious, BYTE SectorPrevious, DWORD dwLBAPrevious, 							  
							   WORD wLength, BYTE *pBuf)
{

	BOOL result; 
	DWORD dwRWSize;
	ATA_PASS_THROUGH_EX *papt; 
	BYTE *ATA_Buffer = new BYTE[sizeof(ATA_PASS_THROUGH_EX) + wLength]; 

	if(hDevice != INVALID_HANDLE_VALUE)
	{


		papt = (ATA_PASS_THROUGH_EX *)ATA_Buffer; 

		ZeroMemory(papt, sizeof(ATA_Buffer)); 
		papt->Length = sizeof(ATA_PASS_THROUGH_EX); 

		papt->AtaFlags = bATAFlag;

		papt->DataTransferLength = wLength; 
		papt->TimeOutValue = 5; 
		papt->DataBufferOffset = sizeof(ATA_PASS_THROUGH_EX); 
		papt->PathId	= 0;
		papt->Lun		= 0;
		papt->TargetId	= 0;



		if(bATAFlag & ATA_FLAGS_DATA_OUT)//Write
		{
//#define ATA_FLAGS_DATA_IN               (1 << 1)
//#define ATA_FLAGS_DATA_OUT              (1 << 2)
			memcpy(ATA_Buffer + papt->DataBufferOffset, pBuf, wLength);
		}

//Current
		papt->CurrentTaskFile[0]= FeatureCurrent; 
		papt->CurrentTaskFile[1]= SectorCurrent;
		papt->CurrentTaskFile[2]= LOBYTE(LOWORD(dwLBACurrent));
		papt->CurrentTaskFile[3]= HIBYTE(LOWORD(dwLBACurrent));
		papt->CurrentTaskFile[4]= LOBYTE(HIWORD(dwLBACurrent));
		papt->CurrentTaskFile[5]= Device | HIBYTE(HIWORD(dwLBACurrent));
		papt->CurrentTaskFile[6]= Command;
		papt->CurrentTaskFile[7]= 0x00; 
//Previous 48bits
		papt->PreviousTaskFile[0]= FeaturePrevious; 
		papt->PreviousTaskFile[1]= SectorPrevious;
		papt->PreviousTaskFile[2]= LOBYTE(LOWORD(dwLBAPrevious));
		papt->PreviousTaskFile[3]= HIBYTE(LOWORD(dwLBAPrevious));
		papt->PreviousTaskFile[4]= LOBYTE(HIWORD(dwLBAPrevious));
		papt->PreviousTaskFile[5]= 0x00;
		papt->PreviousTaskFile[6]= 0x00;
		papt->PreviousTaskFile[7]= 0x00; 

		result = DeviceIoControl( hDevice, 
								  IOCTL_ATA_PASS_THROUGH, 
								  papt, sizeof(ATA_PASS_THROUGH_EX) + papt->DataTransferLength, 
								  papt, sizeof(ATA_PASS_THROUGH_EX) + papt->DataTransferLength, 
								  &dwRWSize, NULL );

		if(result)
		{
			if(bATAFlag & ATA_FLAGS_DATA_IN)//Read
			{
				memcpy(pBuf, &ATA_Buffer[papt->DataBufferOffset], wLength);
			}

			delete ATA_Buffer;
			return result;
		}
		
	}

	delete ATA_Buffer;
	return FALSE;

}

#if(0)
BOOL UpdateATAStrings(HANDLE hDevice, char *pSerialNumber, char *pFW_Version, char* pModelNumber)
{
	BYTE upd_flag = 0;
	BYTE pBuf[512];

	memset(pBuf, 0, 512);
	DLMCHeader *dlmchd = (DLMCHeader *)pBuf;

	

	if(pSerialNumber) { upd_flag |= 0x01; memcpy(dlmchd->serialnum,   pSerialNumber, min(20, strlen((char*)pSerialNumber)));}
	if(pFW_Version)   { upd_flag |= 0x02; memcpy(dlmchd->firmversion, pFW_Version,   min(8,  strlen((char*)pFW_Version)));  }
	if(pModelNumber)  { upd_flag |= 0x04; memcpy(dlmchd->modelnum,    pModelNumber,  min(40, strlen((char*)pModelNumber))); }

	if(upd_flag == 0) return 0xFF;


	dlmchd->signature	= DLMC_ASMT_SIG;
	dlmchd->asmd_sig_1	= ASMT_SIG1;
	dlmchd->asmd_sig_2	= ASMT_SIG2;
	dlmchd->revision	= REVISION;
	dlmchd->command		= CMD_ATA_STRING_CFG;
	dlmchd->sub_command = upd_flag;

	BYTE  ReadWrite	= RW_WRITE;
	WORD  wLength	= 512;
	BYTE  Feature	= 0;
	BYTE  SectorCnt	= 1;
	DWORD dwLBA		= 0;
	BYTE  DevHead	= 0xE0;
	BYTE  Command	= 0x92;

	BYTE status;

	status = SendATAPassThrough(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuf);
	//status = SendATAPassThroughDirect(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuf);

	return status;
}

BOOL  DownloadMicroCodeGetFlashStatusStart(HANDLE hDevice, BYTE StatusFunction)
{
	BYTE status;

	BYTE pBuf[BUFFER_SIZE];
	memset(pBuf, 0, BUFFER_SIZE);

	DLMCHeader * dlmchd = (DLMCHeader *) pBuf;		//convert the buffer[] to download microcode header type through the bufferPtr

	dlmchd->signature	= DLMC_ASMT_SIG;
	dlmchd->asmd_sig_1	= ASMT_SIG1;
	dlmchd->asmd_sig_2	= ASMT_SIG2;
	dlmchd->revision	= REVISION;
	dlmchd->command		= CMD_GET_STS;
	dlmchd->sub_command = SUB_CMD_GET_STS_START;
	dlmchd->function	= StatusFunction;

	BYTE ReadWrite	= RW_WRITE;
	WORD wLength	= 512;
	BYTE Feature	= 0;
	BYTE SectorCnt	= 1;
	DWORD dwLBA		= 0;
	BYTE DevHead	= 0xE0;
	BYTE Command	= 0x92;
	

	status = SendATAPassThrough(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuf);
	//status = SendATAPassThroughDirect(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuf);

	return status;
}

BOOL DownloadMicroCodeGetFlashStatusEnd(HANDLE hDevice)
{
	BYTE status;

	BYTE pBuf[BUFFER_SIZE];
	memset(pBuf, 0, BUFFER_SIZE);

	DLMCHeader * dlmchd = (DLMCHeader *) pBuf;		//convert the buffer[] to download microcode header type through the bufferPtr

	dlmchd->signature	= DLMC_ASMT_SIG;
	dlmchd->asmd_sig_1	= ASMT_SIG1;
	dlmchd->asmd_sig_2	= ASMT_SIG2;
	dlmchd->revision	= REVISION;
	dlmchd->command		= CMD_GET_STS;
	dlmchd->sub_command = SUB_CMD_GET_STS_END;
	

	BYTE ReadWrite	= RW_WRITE;
	WORD wLength	= 512;
	BYTE Feature	= 0;
	BYTE SectorCnt	= 1;
	DWORD dwLBA		= 0;
	BYTE DevHead	= 0xE0;
	BYTE Command	= 0x92;


	status = SendATAPassThrough(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuf);
	//status = SendATAPassThroughDirect(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pBuf);


	return status;
}

BOOL DownloadMicroCodeGetFlashStatusReadSector(HANDLE hDevice, BYTE *pSaveBuf)
{

	BYTE ReadWrite;
	WORD wLength;
	BYTE Feature;
	BYTE SectorCnt;
	DWORD dwLBA;
	BYTE DevHead;
	BYTE Command;
	BOOL status;
	DWORD dwCurSector = 0;

	ReadWrite	= RW_READ;
	wLength		= 512;
	Feature		= 0;
	SectorCnt	= 1;
	dwLBA		= 0;
	DevHead		= 0xE0;
	Command		= 0x20;


	status = SendATAPassThrough(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pSaveBuf);
	//status = SendATAPassThroughDirect(hDevice, ReadWrite, wLength, Feature, SectorCnt, dwLBA, DevHead, Command, pSaveBuf);
	
	return status;

}

BOOL GetFlashStatus(HANDLE hDevice, BYTE function, BYTE *pBuf)
{	
//	    #define     FUN_FW_VERSION		        0x0001	      //current fw version
//		#define	    FUN_CUR_FLSH_CFG	        0x0002	      //current flash configuration
//		#define	    FUN_BAD_BLK_NUM				0x0003	      //bad block number with percentage

	if(!DownloadMicroCodeGetFlashStatusStart(hDevice, function))
		return FALSE;

	if(!DownloadMicroCodeGetFlashStatusReadSector(hDevice, pBuf))
		return FALSE;

	if(!DownloadMicroCodeGetFlashStatusEnd(hDevice))
		return FALSE;

	return TRUE;
	
}

#endif